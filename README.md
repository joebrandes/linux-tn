# Linux Stuff for Trainer and Trainees
Various Stuff for Linux - Feel free to use it for whatever implementations.

In general and at this very second i write these lines everything is **free**: 

*   these informations,
*   the use of it, 
*   the software as i found it (see extra License Infos), ... .

I call these Codes **Snippets**. I had them in plain text files and decided in 2020 to convert them
into this and other Git-Repos. You will find ton of stuff similar to this on the *Net*: dotfiles, configs, ...

**Important - very IMPORTANT ;-)**

>They are (mostly) not usable as 1-to-1 copies to your own environment.
>You should see them as Templates to **choose** and **pick** selected content and/or lines from.

## Getting Started

You don't need an account for this public Repository - feel free to clone it to your own working space.

```
git clone https://gitlab.com/joebrandes/linux-tn.git
```

This - obviously - creates a new folder *linux-tn* and and the rest of the git-structure as subfolders.

These subfolders - sometimes - have some explanations integreted how to use the codes
or some link(s) for further infos and directives.

## Links

* [Joe Brandes](http://www.joe-brandes.de) - Web *Card* by Joe Brandes
* [Git](https://www.git-scm.com/) - Git Development

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc..
