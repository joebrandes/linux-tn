# cmatrix 4 openSUSE

Funny **cmatrix** not in openSUSE Repos - so just do the following:

[link zur Uraltversion](http://rpm.pbone.net/index.php3/stat/4/idpl/23880923/dir/opensuse/com/cmatrix-1.2a-315.2.x86_64.rpm.) -  eine uuuuuuuurrrrallllllteeeeeee cmatrix version für suse 11.4 contrib Repository!!!!

Or just look in this repo! Then simply do:

    $ sudo rpm -i cmatrix-1.2a-315.2.x86_64.rpm
    warning: cmatrix-1.2a-315.2.x86_64.rpm: Header V3 DSA/SHA1 Signature, key ID 62b21ea4: NOKEY
    error: Failed dependencies:
    libncurses.so.5()(64bit) is needed by cmatrix-1.2a-315.2.x86_64

also noch fehlende Bibliothek nachinstallieren:

    sudo zypper install libncurses5

Voila! And obviously you could firstly check the *libncurses5* install and then install the ``cmatrix-1.2a-315.2.x86_64.rpm package`` ;-)

And don't forget to use the real cmatrix Feeling with *asynchronous* mode:

    cmatrix -a


## Links / Technologies

* [Joe Brandes](http://www.joe-brandes.de) - Web *Card* by Joe Brandes
* [Git](https://www.git-scm.com/) - Git Development

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
