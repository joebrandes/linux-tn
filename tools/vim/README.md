# VIM

Quite Simply **The EDITOR** ;-)

![Vim colorscheme gruvbox](./vim-gruvbox.png)

Again - just snippets to build your **own Vim**


## Vim with VimPlug Installation

* Install VimPlug Toolkit:

    `curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim`

* Move Example Config for vimplug to `~/.vimrc`

* Start `vim` - there may be Messages / Errors!

* Do `:PlugInstall` and wait for the VimPlug Magic

* Restart `vim`


## Links / Technologies

* [Joe Brandes](http://www.joe-brandes.de) - Web *Card* by Joe Brandes
* [Git](https://www.git-scm.com/) - Git Development

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
