# Docker install for Debian 11 Bullseye

Vorschlag Trainee Docker Seminar: einfach die Schnellanleitung bereitstellen:

[Docker Debian 11 install auf docs.docker.com](https://docs.docker.com/engine/install/debian/)

Die Commands arbeiten mit `sudo` und lassen sich komplett `copy & paste` nutzen:

    sudo apt-get update
    sudo apt-get install ca-certificates curl gnupg

    sudo install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    sudo chmod a+r /etc/apt/keyrings/docker.gpg

    echo "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

    sudo apt-get update

    sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

Alternativ auch gerne das beigelegte Script nutzen.

