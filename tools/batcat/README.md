# Batcat

or ccat or bat or ...

The idea is: color- and meaningful cat or less tooling.




My Solution: Plugin **colorize** for the ZSH


## Links / Technologies

* [Oh My ZSH Plugin colorize](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/colorize) - Colorize Plugin for ZSH
* [Joe Brandes](http://www.joe-brandes.de) - Web *Card* by Joe Brandes
* [Git](https://www.git-scm.com/) - Git Development

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
