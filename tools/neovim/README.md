# Neovim

I use Vim - so why not use the more modern approach. **Neovim** or **nvim**
is build by a community rather a personal project like **Vim**.

And with compatibility build in for *old* vimscripting and *new* **Lua** Scripting
it is certainly worth a tryout.



## Links / Technologies

* [Neovim](https://neovim.io/) - Website Neovim
* [Joe Brandes](http://www.joe-brandes.de) - Web *Card* by Joe Brandes
* [Git](https://www.git-scm.com/) - Git Development

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
