" Building my NeoVim (nvim) configuration
" Started in 2020

" you need a full color terminal here!
set termguicolors

" --------------------------------------------------------------
" NeoSolarized Colors: https://github.com/overcache/NeoSolarized

" Default value is "normal", Setting this option to "high" or "low" does use the
" same Solarized palette but simply shifts some values up or down in order to
" expand or compress the tonal range displayed.
let g:neosolarized_contrast = "normal"

" Special characters such as trailing whitespace, tabs, newlines, when displayed
" using ":set list" can be set to one of three levels depending on your needs.
" Default value is "normal". Provide "high" and "low" options.
let g:neosolarized_visibility = "normal"

" I make vertSplitBar a transparent background color. If you like the origin
" solarized vertSplitBar style more, set this value to 0.
let g:neosolarized_vertSplitBgTrans = 1

" If you wish to enable/disable NeoSolarized from displaying bold, underlined
" or italicized" typefaces, simply assign 1 or 0 to the appropriate variable.
" Default values:
let g:neosolarized_bold = 1
let g:neosolarized_underline = 1
let g:neosolarized_italic = 0

" Used to enable/disable "bold as bright" in Neovim terminal. If colors of bold
" text output by commands like `ls` aren't what you expect, you might want to
" try disabling this option. Default value:
let g:neosolarized_termBoldAsBright = 1

set background=dark
colorscheme NeoSolarized

" NeoSolarized Colors
" --------------------------------------------------------------


" -----------------------------------------------------------
" FILE BROWSING: (changed with https://shapeshed.com/vim-netrw/)
 
" Tweaks for browsing (auch on https://shapeshed.com/vim-netrw/)
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
" PLUS: https://shapeshed.com/vim-netrw/
" let g:netrw_winsize = 25
" JoeB: lädt automatisch Netrw:
" augroup ProjectDrawer
"  autocmd!
"  autocmd VimEnter * :Vexplore
" augroup END
 
" Extras: (aus Anleitung Vim without Plugins)
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
 
" NOW WE CAN:
" - :edit a folder to open a file browser
" - <CR>/v/t to open in an h-split/v-split/tab
" - check |netrw-browse-maps| for more mappings
" - JoeB: oder :Vexplore :Sexplore ...
" -----------------------------------------------------------


" -----------------------------------------------------------
" MYSTUFF: Plugins with Pathogen
"-----------------------------------------------------------
"mkdir -p ~/.vim/autoload ~/.vim/bundle && \
"curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
"-----------------------------------------------------------
"If you're using Windows, change all occurrences of ~/.vim to ~\vimfiles.
"-----------------------------------------------------------
"Plugin-list: see folder .vim/bundle and www.vimawesome.com
"cd ~/.vim/bundle
"git clone https://github.com/mattn/emmet-vim
"git clone https://github.com/morhetz/gruvbox
"git clone https://github.com/vim-airline/vim-airline
"git clone https://github.com/vim-airline/vim-airline-themes
"git clone https://github.com/altercation/vim-colors-solarized
"git clone https://github.com/tpope/vim-fugitive
"-----------------------------------------------------------
execute pathogen#infect()


" SETTINGS:

" JoeB: see https://vim.fandom.com/wiki/Omni_completion
set omnifunc=syntaxcomplete#Complete
 
" Character Encoding with UTF-8
set encoding=utf8
 
" line numbers and relative lines for jumps like 14k
set number
set relativenumber
 
" Highlight in Search; Switch off :nohlsearch
set hlsearch
 
" Always keep 10 lines visible above and below cursor
set scrolloff=10
 
" Tabbing Indent using four spaces
set tabstop=4
set softtabstop=4
set shiftwidth=4
 
" Convert tabs to spaces
set expandtab
 
" ignore cases / no casesensitivity
set ignorecase
" overrides ignorecase for /, ?, n, N, :g, :s
set smartcase
 
" New lines inherit the indentation of previous lines
set autoindent
set smartindent
 
" show comand Keys in Status line
set showcmd

" AIRLINE STUFF:
let g:airline_solarized_bg='dark'
let g:airline_powerline_fonts = 1
