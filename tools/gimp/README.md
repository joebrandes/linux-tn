# gimp

Yes in know - **Gimp** is no Photoshop, but it can easily look like it!

![Gimp tweaked like Photoshop](./gimp-mit-ps-tweaks-2.10.png)

(**german source**) Anleitung in Linux Welt 05/2020
Softwaretipps - Seite 110 - Gimp: Näher an Adobe Photoshop

**curl** und **git** werden benötigt / needed

git Projekt auf: https://github.com/doctormo/GimpPs.git

Your own GIMP ``/home/joeb/.config/GIMP/2.10`` will be in own Backup-Folder!

Let's do per oneliner:

    sh -c "$(curl -fsSL https://raw.githubusercontent.com/arbaes/GimpPs/master/tools/install.sh)"

If you wanna check out, what the script ``install.sh`` will do just download it previously:

    curl -fsSL https://raw.githubusercontent.com/arbaes/GimpPs/master/tools/install.sh


## Links / Technologies

* [Gimp like Photoshop](https://github.com/doctormo/GimpPs.git) - Git Project
* [Gimp](http://gimp.org) - Gimp on the web
* [Joe Brandes](http://www.joe-brandes.de) - Web *Card* by Joe Brandes
* [Git](https://www.git-scm.com/) - Git Development

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
