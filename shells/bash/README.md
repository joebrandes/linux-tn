# Bash

The basic idea is to get a good looking and good working Shell Environment - here **Bash**
the **Bourne Again Shell**, which is the Standard-Shell on most of our Linuxes.

![Bash and rxvt solarized dark styled](./bash-and-rxvt-solarized.png)

And obviously we want Git-Support to use full advantage of *Gitting* in our terminals.

And even more obviously: i like **Solarized Dark** Styling.


## Links / Technologies

* [Powerline-Go](https://github.com/justjanne/powerline-go) - Powerline-Go by Janne Mareike Koschinski justjanne - attention: it needs a relatively up to date go version!
* [Joe Brandes](http://www.joe-brandes.de) - Web *Card* by Joe Brandes
* [Git](https://www.git-scm.com/) - Git Development

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
