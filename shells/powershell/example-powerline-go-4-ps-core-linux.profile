# Aliase

# https://sourceforge.net/projects/mcwin32/ 
# Set-Alias mc "C:\programme_ni\mcwin32-build204-bin\mc.exe"
# Linux confused ;-)
Set-Alias ll ls

# PowerShell powerline-go stuff
# see: https://docs.microsoft.com/de-de/windows/terminal/tutorials/powerline-setup
# do:
# Install-Module posh-git -Scope CurrentUser
# Install-Module oh-my-posh -Scope CurrentUser
# if on PowerShell core:
# Install-Module -Name PSReadLine -AllowPrerelease -Scope CurrentUser -Force -SkipPublisherCheck

Import-Module posh-git
Import-Module oh-my-posh
Set-Theme PowerLine
