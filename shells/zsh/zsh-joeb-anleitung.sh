# open the plain fresh installed zsh opens a configuration setting:
# -----------------------------------------------------------------
# This is the Z Shell configuration function for new users,
# zsh-newuser-install.
# You are seeing this message because you have no zsh startup files
# (the files .zshenv, .zprofile, .zshrc, .zlogin in the directory
# ~).  This function can help you with a few settings that should
# make your use of the shell easier.
#
# You can:
#
# (q)  Quit and do nothing.  The function will be run again next time.
#
# (0)  Exit, creating the file ~/.zshrc containing just a comment.
#      That will prevent this function being run again.
#
# (1)  Continue to the main menu.
#
# (2)  Populate your ~/.zshrc with the configuration recommended
#      by the system administrator and exit (you will need to edit
#      the file by hand, if so desired).
#
# --- Type one of the keys in parentheses --- 
# -----------------------------------------------------------------
# Standard .zshrv example: .zshrc-standard-20200914


# let's start with the fun: oh-my-zsh 
# zsh with oh-my-zsh Plugin management
# https://github.com/ohmyzsh/ohmyzsh
# bzw.
# https://github.com/ohmyzsh/ohmyzsh
# oneliner creates $HOME/.oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
# creates default .zshrc with plugins=(git)
# old .zshrc backed in /tmp/.zshrc.asdfasdf
# new Lines at beginning (p10k-instant-prompt...) and end (.p10k.zsh sourcen)
#
# Historyfile: $HOME/.zsh_history
#
# All the Plugins:
# https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins


# powerlevel10k - powerline plugin for various Shells
# https://github.com/romkatv/powerlevel10k#oh-my-zsh
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
# Activate Theme with change of Theme
ZSH_THEME="powerlevel10k/powerlevel10k"
# first start creates: .p10k.zsh
# changes / new configuration with: p10k configure


# zsh-autosuggestions
# https://github.com/zsh-users/zsh-autosuggestions
# makes suggestions in prompt for things typed in zsh previously
# It suggests commands as you type based on history and completions.
# https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md#oh-my-zsh
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
# in .zshrc do:  plugins=(git zsh-autosuggestions ... )


# zsh-completions
# completion for zsh
# https://github.com/zsh-users/zsh-completions
git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-completions
# plugins=(git zsh-autosuggestions ... zsh-completions)
# autoload -U compinit && compinit

# autojump
# do the j to the right and then the j to the left ;-)
# https://github.com/wting/autojump
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/autojump
# ======================================================================
# Installation thru Linux Distro Packages: autojump or autojump-zsh, ...
# ======================================================================
# add the autojump Technology to your .zshrc plugins:
# plugins=(git zsh-autosuggestions zsh-completions ... autojump)

# zsh-syntax-highlighting
# https://github.com/zsh-users/zsh-syntax-highlighting
# https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md
# ATTENTION: let it be last Plugin!
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
# in .zshrc do:  plugins=(git zsh-autosuggestions ... zsh-syntax-highlighting)


# Plugin k for git removed because of Tool exa
# https://the.exa.website/
# for Example:
# exa --long --tree --level=2 ./...
# alias lexa='exa --long --tree --level=2' 


#
# Standard-Plugins oh-my-zsh
# colorize
# --------
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/colorize
# it will provide Tools ccat (aka colorize cat) and cless (colorize less)
# and Variables ZSH_COLORIZE_...
# Standard-Colorizer: pygments (alternative: chroma)
#
# colored-man-pages
# -----------------
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/colored-man-pages
# makes man-Pages more readable and syntax-highlighted
# provides Tool: colored (e.g.: colored git help clone)
#
# alias-finder
# ------------
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/alias-finder
# get me / find me the aliases that match an expression
# --longer -l finds stuff longer
# --exact -e avoids aliases shorter than the expression
#
# vi-mode
# -------
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/vi-mode
# make using zsh vi-like
# with powerlevel10k theme its ">" for insert-mode and "<" for command mode
# commandMode v brings you into Vim
# as always: put vi-mode in plugins()!


# 
# colorls
# =======
# fine colors AND ICONS for the Shell
# https://github.com/athityakumar/colorls
# in short: maybe install ruby-dev package
# sudo gem install colorls
# build manpages with
# sudo gem man -u
# example Alias:
# alias jblc='colorls -lA --sd'
# jbgc='colorls --gs'
# jblc='colorls -lA --sd'
# jbtc='colorls --tree=2'

#
# lf - list files tools for console
# ==
# https://github.com/gokcehan/lf
# linux releases: https://github.com/gokcehan/lf
# rem: no dircolor with solarized dark stuff anymore
#
