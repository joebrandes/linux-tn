..  Zusatzinstallationen auf Ubuntu Büro 2028 Maschine: buero-2018

================
Paketmanagements
================

Alles was auf der Maschine neben den Standardinstallationen installiert/bereitgestellt worden ist!

Eine genauere Auflistung folgt - hier der *OneLiner* für Tools ohne Nachinstallation:

::

    sudo apt install papirus-icon-theme \
    folder-color \
    keepassxc \
    filezilla \
    gparted \
    calibre \
    openssh-server \
    vlc \
    chromium-browser \
    most \
    tree \
    bat \
    fortunes \
    cowsay \
    lolcat \
    neofetch \
    cmatrix \
    terminator \
    zsh zsh-syntax-highlighting autojump zsh-autosuggestions \
    mc \
    vim \
    inxi \
    htop \
    glances \
    tty-clock \
    net-tools \
    python3-pip \
    powerline fonts-powerline golang-go \
    cifs-utils keyutils



Mit Nachinstallationen:

::

    sudo apt install virtualbox virtualbox-ext-pack
    sudo adduser joeb vobxusers
    sudo systemctl status virtualbox.service

RestructuredText Unterstützung:

::

    sudo pip3 install -U Sphinx docutils sphinx sphinx-autobuild doc8 rstcheck sphinx-rtd-theme sphinx-typo3-theme

PDFLatex Unterstützung:

::

    sudo apt install latexmk texlive-ful


DEB-Paketmanagment
==================

Also: die APT-Tool basierte Technik - früher oder später Konzentration auf Desktop Cinnamon
(ansonsten einfach: ``sudo apt install cinnamon cinnamon-desktop-environment``)

**Papirus Icon Theme** - nice Folder Theme with Colors

::

    sudo apt install papirus-icon-theme

**Folder Color** - Erweiterung für Farben

::

    sudo apt install folder-color


**KeepassXC**

::

    sudo apt install keepassxc

**Filezilla**

::

    sudo apt install filezilla

**Gparted**

::

    sudo apt install gparted

**Calibre**

::

    sudo apt install calibre

**openSSH Server**

::

    sudo apt install openssh-server

**VLC** - Video Lan Client - still first choice

::

    sudo apt-get install vlc

**Chromium**

::

    sudo apt install chromium-browser


Installationen mit Nachinstallations-Schritten:

**VirtualBox** - Virtualisierung

::

    sudo apt install virtualbox virtualbox-ext-pack
    sudo adduser joeb vobxusers
    sudo systemctl status virtualbox.service

Konsolentools
-------------

Most (most) - better Pager

Bat (oder BatCat - Ubuntu-Paket: bat) - Befehl: batcat

Fortune Cookies (fortunes fortunes-de)

Cowsay (cowsay) - lass Kühe (oder Tux) sprechen

Lolcat (lolcat) - bring Farbe in Konsole

Neofetch (neofetch) - Systeminfo

CMatrix (cmatrix) - Screenserver Matrix-Style in Konsole

Terminator (terminator) - Multikonsole

Zsh (zsh) - more than Shell aka Bash

sudo apt install zsh zsh-syntax-highlighting autojump zsh-autosuggestions

see: https://christitus.com/zsh/

Midnight Commander (mc)

VIm (vim) - komplettieren

INXI (inxi) - Analysetool

Htop (htop) - Process Tool

Glances (glances) - Systemmonitor

Asciinema (asciinema) - Aufnahme in Konsole

TTY-Clock (tty-clock) - show Date, Time

Net-Tools (net-tools) - netstat, ifconfig (Klassiker), route, arp

Python

Ggf. auf Version 3 umstellen: sudo ln -s /usr/bin/python3 /usr/bin/python

sudo apt install python3-pip python2-pip

Sphinx und Rst

sudo pip3 install -U Sphinx
sudo pip3 install docutils
sudo pip3 install sphinx sphinx-autobuild
sudo pip3 install doc8
sudo pip3 install rstcheck
sudo pip3 install sphinx-rtd-theme
sudo pip3 install sphinx-typo3-theme

PDFLatex Unterstützung:

sudo apt install latexmk texlive-full

Nettere Konsole mit Powerline Go Techniken:

sudo apt install powerline fonts-powerline golang-go

CIFS Unterstützung

sudo apt-get install cifs-utils keyutils








Third-Party Installs
====================

Natürlich *eigentlich* vermeiden - aber Mehrwert und Quellen mit Seriösität und Aktualität!


VSCodium: codium
----------------

**OLD** (new further down) and with 429 Error in August 2020:

`https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo <https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo>`_

Auszug aus Anleitungen: ... Option 2. Add Repo "Manually" - Add my key:

::

    wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/usr/share/keyrings/vscodum-archive-keyring.gpg

Add the repository:

::

    echo 'deb [signed-by=/usr/share/keyrings/vscodum-archive-keyring.gpg] https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee /etc/apt/sources.list.d/vscodium.list

**NEW** with changed gitlab-sources and key

::

    wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium-archive-keyring.gpg  

Repo:

::

    deb [signed-by=/etc/apt/trusted.gpg.d/vscodium-archive-keyring.gpg] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main  


Update then install vscodium: (bzw. codium!)

::

    sudo apt update
    sudo apt install codium

APT-REPO:

::

    cat /etc/apt/sources.list.d/vscodium.list
    deb [signed-by=/usr/share/keyrings/vscodum-archive-keyring.gpg] https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main

Falls der *Marketplace* in VSCODIUM / CODIUM nicht funktioniert:  siehe ``./resources/app/project.json``
`https://github.com/GabrielBB/vscode-lombok/issues/41 <https://github.com/GabrielBB/vscode-lombok/issues/41>`_
::

    # Anzeigen lassen mit:
    cat /usr/share/codium/resources/app/product.json | grep -e 'serviceUrl' -e 'itemUrl'
    # zeigt:
    "serviceUrl": "https://open-vsx.org/vscode/gallery",
    "itemUrl": "https://open-vsx.org/vscode/item"

    # soll werden zu:
    "extensionsGallery": {
        "serviceUrl": "https://marketplace.visualstudio.com/_apis/public/gallery",
        "cacheUrl": "https://vscode.blob.core.windows.net/gallery/index",
        "itemUrl": "https://marketplace.visualstudio.com/items",
        "controlUrl": "https://az764295.vo.msecnd.net/extensions/marketplace.json",
        "recommendationsUrl": "https://az764295.vo.msecnd.net/extensions/workspaceRecommendations.json.gz"
    }

Anmerkung aus Anleitungen: You should be good by simply changing the serviceUrl and the itemUrl.




Veracrypt
---------

`Veracrypt Downloads <https://www.veracrypt.fr/en/Downloads.html>`_

bzw. für Ubuntu:

`https://wiki.ubuntuusers.de/VeraCrypt/ <https://wiki.ubuntuusers.de/VeraCrypt/>`_

mit dem PPA ppa:unit193/encryption

`https://launchpad.net/~unit193/+archive/ubuntu/encryption <https://launchpad.net/~unit193/+archive/ubuntu/encryption>`_

also:

::

    sudo add-apt-repository ppa:unit193/encryption

    cat /etc/apt/sources.list.d/unit193-ubuntu-encryption-focal.list

    deb http://ppa.launchpad.net/unit193/encryption/ubuntu focal main
    # deb-src http://ppa.launchpad.net/unit193/encryption/ubuntu focal main


Nutzung mit Mount-Dir ``/mnt/veracrypt`` und ``mount.cifs`` Tools unter ubuntu gemäß
`https://wiki.ubuntuusers.de/Samba_Client_cifs/ <https://wiki.ubuntuusers.de/Samba_Client_cifs/>`_ :

Install / Install-Check:

::

    sudo apt-get install cifs-utils keyutils

Temporäres Einhängen:

    sudo mount -t cifs -o username=Benutzername //192.168.1.100/Tausch /media/austausch


Gitsudo mount -t cifs -o username=joebds //diskstation/vc2019 /mnt/veracrypt
---

`https://www.git-scm.com/ <https://www.git-scm.com/>`_

`https://www.git-scm.com/download/linux <https://www.git-scm.com/download/linux>`_

PPA: ppa:git-core/ppa


also:

::

    sudo add-apt-repository ppa:git-core/ppa

    cat /etc/apt/sources.list.d/git-core-ubuntu-ppa-focal.list
    deb http://ppa.launchpad.net/git-core/ppa/ubuntu focal main
    # deb-src http://ppa.launchpad.net/git-core/ppa/ubuntu focal main





SNAP
====

Anm.: versuchen zu vermeiden ;-) - bei Linux Mint kein Snap-Store!

Installationen einsehen mit ``snap list``


AppImage
========

Just Download and run... ;-)

OneNote
-------

`P3X OneNote Electron APP <https://www.electronjs.org/apps/p3x-onenote>`_


