# linux mint 19 installs:
sudo apt install keepassxc \
    filezilla \
    gparted \
    calibre \
    openssh-server \
    vlc \
    chromium-browser \
    most \
    fortunes \
    fortune-mod \
    cowsay \
    lolcat \
    neofetch \
    cmatrix \
    sl \
    terminator \
    mc \
    vim \
    inxi \
    htop \
    glances \
    tty-clock \
    net-tools \
    python3-pip \
    powerline fonts-powerline golang-go \
    cifs-utils keyutils

