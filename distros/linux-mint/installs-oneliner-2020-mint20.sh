# git bereitstellen:
add-apt-repository ppa:git-core/ppa 
apt update
apt install git

# linux mint 20 installs:
sudo apt install keepassxc \
    filezilla \
    gparted \
    calibre \
    openssh-server \
    vlc \
    most \
    fortunes fortune-mod \
    cowsay \
    lolcat \
    neofetch \
    cmatrix \
    terminator \
    mc \
    vim \
    inxi \
    htop \
    glances \
    tty-clock \
    net-tools \
    python3-pip \
    powerline fonts-powerline golang-go \
    cifs-utils keyutils \
    nemo-terminal nemo-audio-tab nemo-compare nemo-extension-fma nemo-image-converter nemo-media-columns

# if necessary: get me some go Scripting https://golang.org/dl/
sudo tar -C /usr/local -xzf go1.14.4.linux-amd64.tar.gz
# get PATH going in /etc/profile (for a system-wide installation) or $HOME/.profile:
export PATH=$PATH:/usr/local/go/bin


# serving the go stuff for powerline in home-Dir ~ / $HOME
go get -u github.com/justjanne/powerline-go

# append in ~/.bashrc or copy the complete Stuff for linux-systems offered separately
export GOPATH="$HOME/go"

function _update_ps1() {
    PS1="$($GOPATH/bin/powerline-go -error $? -newline -theme default -shell bash -modules-right time)"
}

if [ "$TERM" != "linux" ] && [ -f "$GOPATH/bin/powerline-go" ]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi

# don't forget to copy dircolors.256dark file into $HOME
eval `dircolors -b dircolors.256dark`

