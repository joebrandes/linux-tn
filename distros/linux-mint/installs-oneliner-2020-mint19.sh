# git bereitstellen:
add-apt-repository ppa:git-core/ppa 
apt update
apt install git

# linux mint 19 installs:
sudo apt install -y papirus-icon-theme \
    folder-color \
    keepassxc \
    filezilla \
    gparted \
    calibre \
    openssh-server \
    vlc \
    chromium-browser \
    most \
    batcat \    
    fortunes fortune-mod \
    cowsay \
    lolcat \
    neofetch \
    cmatrix \
    terminator \
    mc \
    vim \
    inxi \
    htop \
    glances \
    tty-clock \
    net-tools \
    python3-pip \
    powerline fonts-powerline golang-go \
    cifs-utils keyutils


