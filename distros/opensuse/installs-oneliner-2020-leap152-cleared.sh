# Standardinstalls:

zypper install tree \
    keepassxc \
    filezilla \
    gparted \
    calibre \
    vlc \
    bat \
    fortune \
    cowsay \
    neofetch \
    terminator \
    sl \
    mc \
    vim \
    inxi \
    htop \
    net-tools \
    python3-pip \
    powerline powerline-fonts go go-doc \
    cifs-utils keyutils


# Mit Nachinstallationen (falls HOST):

zypper instal virtualbox virtualbox-ext-pack
usermod -aG joeb vobxusers
systemctl status virtualbox.service

# als Virtualbox-Guest: (checken)
zypper install virtualbox-guest-tools virtualbox-guest-x11 virtualbox-guest-kmp-default

# RestructuredText Unterstützung: (ggf. python version/s checken: python --version, ...)
sudo pip3 install -U Sphinx docutils sphinx sphinx-autobuild doc8 rstcheck sphinx-rtd-theme sphinx-typo3-theme

# PDFLatex Unterstützung:
zypper install latexmk texlive-ful
