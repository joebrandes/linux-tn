# Standardinstalls:

zypper install -y papirus-icon-theme \
    # not found: folder-color \
    keepassxc \
    filezilla \
    gparted \
    calibre \
    # already installed: openssh-server \
    vlc \
    # not found: chromium-browser \
    # not found: most \
    bat \
    fortune \
    cowsay \
    # not found: lolcat \
    neofetch \
    # not found: cmatrix \
    terminator \
    # zsh zsh-syntax-highlighting autojump zsh-autosuggestions \
    mc \
    vim \
    inxi \
    htop \
    # not found: glances \
    # not found: tty-clock \
    net-tools \
    python3-pip \
    powerline powerline-fonts go go-doc \
    cifs-utils keyutils


# Mit Nachinstallationen (falls HOST):

zypper instal virtualbox virtualbox-ext-pack
usermod -aG joeb vobxusers
systemctl status virtualbox.service

# als Guest:
zypper install virtualbox-guest-tools virtualbox-guest-x11 virtualbox-guest-kmp-default

# RestructuredText Unterstützung:
zypper pip3 install -U Sphinx docutils sphinx sphinx-autobuild doc8 rstcheck sphinx-rtd-theme sphinx-typo3-theme

# PDFLatex Unterstützung:
zypper install latexmk texlive-ful
