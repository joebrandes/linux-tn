# Linux Installs

Just some ideas for stuff to install on different platforms.

Not always tested and not even close to complete - especially for the WM stuff.



## Links / Technologies

* [Joe Brandes](http://www.joe-brandes.de) - Web *Card* by Joe Brandes
* [Git](https://www.git-scm.com/) - Git Development

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
