# Standardinstalls:

sudo apt install papirus-icon-theme \
    folder-color \
    keepassxc \
    filezilla \
    gparted \
    calibre \
    openssh-server \
    vlc \
    chromium-browser \
    most \
	exuberant-ctags \
    bat \
    fortunes \
    cowsay \
    lolcat \
    neofetch \
    cmatrix \
    terminator \
    zsh zsh-syntax-highlighting autojump zsh-autosuggestions \
    mc \
    vim \
    inxi \
    htop \
    glances \
    tty-clock \
    net-tools \
    python3-pip \
    powerline fonts-powerline golang-go \
    cifs-utils keyutils


# Mit Nachinstallationen:

sudo apt install virtualbox virtualbox-ext-pack
sudo adduser joeb vobxusers
sudo systemctl status virtualbox.service

# RestructuredText Unterstützung:

sudo pip3 install -U Sphinx docutils sphinx sphinx-autobuild doc8 rstcheck sphinx-rtd-theme sphinx-typo3-theme

# PDFLatex Unterstützung:

sudo apt install latexmk texlive-ful
