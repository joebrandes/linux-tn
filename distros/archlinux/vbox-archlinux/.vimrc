" JoeB: documented version - easyly slimmed with grep
" Many Ideas and stuff for --vim with no_plugins-- from
" https://github.com/changemewtf/no_plugins 
" https://www.youtube.com/watch?v=XA2WjJbmmoM
" Let's call it vanilla vim
"Slim-Down-Version (without wanted Commentary) with
"egrep -v '^" ' .vimrc | egrep -v "^\s*$"

" enter the current millenium
set nocompatible
 
" enable syntax and plugins (for netrw)
syntax enable
filetype plugin on

" -----------------------------------------------------------
" Search down into subfolders
" Provides tab-completion for all file-related tasks
set path+=**
 
" Display all matching files when we tab complete
set wildmenu
" JoeB: Wildmenu Stuff
set wildmode:list:full
set wildignore+=tags,_build,.vscode,node_modules,.pyc,.swp
 
" NOW WE CAN:
" JoeB: use :find install*.rst e.g.
" - JoeB - Folder-Exclude: set wildignore+=**/node_modules/** 
" - Hit tab to :find by partial match
" - Use * to make it fuzzy
 
" THINGS TO CONSIDER:
" - :b lets you autocomplete any open buffer
" JoeB: use Buffer - no Windows / no Tabs!
" -----------------------------------------------------------

" -----------------------------------------------------------
" JoeB: for Programming-Projects
" TAG JUMPING: (JoeB: needs exuberant-ctags package - file tags easy > 100e MiB!)
 
" Create the `tags` file (may need to install ctags first)
" command! MakeTags !ctags -R .
 
" NOW WE CAN: (JoeB: needs definitely remapping!)
" - Use ^] to jump to tag under cursor
" - Use g^] for ambiguous tags
" - Use ^t to jump back up the tag stack
 
" THINGS TO CONSIDER:
" - This doesn't help if you want a visual list of tags
" -----------------------------------------------------------

" -----------------------------------------------------------
" AUTOCOMPLETE:
" The good stuff is documented in |ins-completion|
 
" HIGHLIGHTS:
" - ^x^n for JUST this file
" - ^x^f for filenames (works with our path trick!)
" - ^x^] for tags only
" - ^n for anything specified by the 'complete' option
" - JoeB: ^e for no Topic and back to Text 
" NOW WE CAN:
" - Use ^n and ^p to go back and forth in the suggestion list
 
" JoeB: I recommened also omni completion with
" Link https://vim.fandom.com/wiki/Omni_completion needs:
" filetype plugin on
" set omnifunc=syntaxcomplete#Complete
" - ^x^o for specific omni completion (SQL, HTML, CSS, JS, PHP,...)
" ----------------------------------------------------------

" -----------------------------------------------------------
" FILE BROWSING: (changed with https://shapeshed.com/vim-netrw/)
 
" Tweaks for browsing (auch on https://shapeshed.com/vim-netrw/)
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
" PLUS: https://shapeshed.com/vim-netrw/
" let g:netrw_winsize = 25
" JoeB: lädt automatisch Netrw:
" augroup ProjectDrawer
"  autocmd!
"  autocmd VimEnter * :Vexplore
" augroup END
 
" Extras: (aus Anleitung Vim without Plugins)
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
 
" NOW WE CAN:
" - :edit a folder to open a file browser
" - <CR>/v/t to open in an h-split/v-split/tab
" - check |netrw-browse-maps| for more mappings
" - JoeB: oder :Vexplore :Sexplore ...
" -----------------------------------------------------------

" -----------------------------------------------------------
" SNIPPETS:
 
" Read an empty HTML template and move cursor to title
" nnoremap ,html :-1read $HOME/.vim/.skeleton.html<CR>3jwf>a

" JoeB: Explaination please
" n - für Normal Mode
" noremap - no use if ,html called; techn.: no reinvoke
" , - Leader Key (otherwise :)
" html - my Map-Name
" : - enter Commandmode
" -1 - one line less
" read - lies/read
" $HOME/.vim/.skeleton.html - filename
" <CR> - CarriageReturn so back in Normal Mode
" 3jwf>a - Positioning into Position ;-)

" NOW WE CAN:
" - Take over the world!
"   (with much fewer keystrokes)
" ----------------------------------------------------------

" -----------------------------------------------------------
" MYSTUFF: Plugins with Pathogen
"-----------------------------------------------------------
"mkdir -p ~/.vim/autoload ~/.vim/bundle && \
"curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
"-----------------------------------------------------------
"If you're using Windows, change all occurrences of ~/.vim to ~\vimfiles.
"-----------------------------------------------------------
"Plugin-list: see folder .vim/bundle and www.vimawesome.com
"cd ~/.vim/bundle
"git clone https://github.com/mattn/emmet-vim
"git clone https://github.com/morhetz/gruvbox
"git clone https://github.com/vim-airline/vim-airline
"git clone https://github.com/vim-airline/vim-airline-themes
"git clone https://github.com/altercation/vim-colors-solarized
"git clone https://github.com/tpope/vim-fugitive
"-----------------------------------------------------------
execute pathogen#infect()

" SETTINGS:

" JoeB: see https://vim.fandom.com/wiki/Omni_completion
set omnifunc=syntaxcomplete#Complete

" Character Encoding with UTF-8
set encoding=utf8

" line numbers and relative lines for jumps like 14k
set number
set relativenumber

" Highlight in Search; Switch off :nohlsearch
set hlsearch

" Always keep 10 lines visible above and below cursor
set scrolloff=10

" Tabbing Indent using four spaces
set tabstop=4
set softtabstop=4
set shiftwidth=4

" Convert tabs to spaces
set expandtab

" ignore cases / no casesensitivity
set ignorecase
" overrides ignorecase for /, ?, n, N, :g, :s
set smartcase
 
" New lines inherit the indentation of previous lines
set autoindent
set smartindent

" show comand Keys in Status line
set showcmd

" Background should ALWAYS be DARK ;-)
set background=dark

" SOLARIZED Theme STUFF:
" let g:solarized_termcolors=256
" colorscheme solarized

" GRUVBOX Theme STUFF:
colorscheme gruvbox

" AIRLINE STUFF:
let g:airline_solarized_bg='dark'
let g:airline_powerline_fonts = 1
 
