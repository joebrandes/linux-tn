# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


# ANTIGEN Solution for Plugins:
# https://github.com/zsh-users/antigen/wiki/Quick-start 
source $HOME/antigen.zsh

antigen use oh-my-zsh

antigen bundle git
antigen bundle colorize
antigen bundle colored-man-pages
antigen bundle alias-finder
antigen bundle zsh-users/zsh-completions
antigen bundle z
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions

#antigen theme fox
# antigen theme robbyrussell
antigen theme romkatv/powerlevel10k

antigen apply


# completions for tool lf in fpath:
fpath=($HOME/opt/lf-linux-amd64 $fpath)
# use completions:
autoload -U compinit && compinit


# fzf
#. /usr/share/doc/fzf/examples/key-bindings.zsh
#. /usr/share/doc/fzf/examples/completion.zsh
#export FZF_DEFAULT_OPTS='--height 40% --border'
#export FZF_DEFAULT_COMMAND='rg --files --hidden -g !.git/'
##export FZF_DEFAULT_COMMAND='find ~ -name .git -prune -o -name tmp -prune -o -type f,d -print'
#export FZF_ALT_C_COMMAND='find .'
#export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

# using ripgrep
export RIPGREP_CONFIG_PATH="$HOME/.ripgreprc"
# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#

#
# Plugin colorize provides tools colorize_cat (alias ccat) and colorize_less (cless):
if [ -n "$(command -v colorize_cat)" ] && [ -n "$(command -v colorize_less)" ]; then
    alias cat='colorize_cat'
    alias less='colorize_less'
fi

# use exa tool:
if [ -x "$(command -v exa)" ]; then
    alias lx='exa --long --tree --level=1 --git'
fi

# use colorls tool:
#if [ -x "$(command -v colorls)" ]; then
#    alias lc='colorls -lA --sd'
#fi

# Completion for tool colorls
#source $(dirname $(gem which colorls))/tab_complete.sh

# set alias lc for colorls
#alias lc='colorls -lAh --sd'

# lf - list files with previewer lfimg
alias lf='lfrun'

# set dircolors with solarized dark
# eval $(dircolors -b ~/.bash_dircolors.256dark)

# put my Aliases here - later in extra file
# -----------------------------------------

# Passwords mystyle with Tool makepasswd: 15 Characters out of string
if [ -x "$(command -v makepasswd)" ]; then
    alias jbmakepasswd="makepasswd --chars 15 --string 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVXYZ#+-()=}][{;:'"
fi

# imagemagick mogrify tool for web-pics 800px
if [ -x "$(command -v mogrify)" ]; then
    alias jbmogrify="mogrify -path _web-800px/ -resize 800 -quality 90 -format jpg *.png"
fi

# vifm with ueberzug
alias vifm="~/.config/vifm/vifmrun"

# using ranger with urxvt 
#if [ -x "$(command -v ranger)" ]; then
#    alias ranger="urxvt -e ranger"
#fi

# ZSH Colorize Style
export ZSH_COLORIZE_STYLE="native"
# alternative Editor
export MICRO_TRUECOLOR=1

# fixing Exports for RANGER standard Editor vim
export VISUAL=vim

# ghostscript gs function for pdfmerge
pdfmerge() {  
    gs -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -o $@;  
}  

if [ "$TERM" = "linux" ]; then
	printf %b '\e[40m' '\e[8]' # set default background to color 0 'dracula-bg'
	printf %b '\e[37m' '\e[8]' # set default foreground to color 7 'dracula-fg'
	printf %b '\e]P0282a36'    # redefine 'black'          as 'dracula-bg'
	printf %b '\e]P86272a4'    # redefine 'bright-black'   as 'dracula-comment'
	printf %b '\e]P1ff5555'    # redefine 'red'            as 'dracula-red'
	printf %b '\e]P9ff7777'    # redefine 'bright-red'     as '#ff7777'
	printf %b '\e]P250fa7b'    # redefine 'green'          as 'dracula-green'
	printf %b '\e]PA70fa9b'    # redefine 'bright-green'   as '#70fa9b'
	printf %b '\e]P3f1fa8c'    # redefine 'brown'          as 'dracula-yellow'
	printf %b '\e]PBffb86c'    # redefine 'bright-brown'   as 'dracula-orange'
	printf %b '\e]P4bd93f9'    # redefine 'blue'           as 'dracula-purple'
	printf %b '\e]PCcfa9ff'    # redefine 'bright-blue'    as '#cfa9ff'
	printf %b '\e]P5ff79c6'    # redefine 'magenta'        as 'dracula-pink'
	printf %b '\e]PDff88e8'    # redefine 'bright-magenta' as '#ff88e8'
	printf %b '\e]P68be9fd'    # redefine 'cyan'           as 'dracula-cyan'
	printf %b '\e]PE97e2ff'    # redefine 'bright-cyan'    as '#97e2ff'
	printf %b '\e]P7f8f8f2'    # redefine 'white'          as 'dracula-fg'
	printf %b '\e]PFffffff'    # redefine 'bright-white'   as '#ffffff'
	clear
fi


#To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
