-- ------------------------------------------------------------------------
--      _              ____    ____  _          __  __ 
--     | | ___   ___  | __ )  / ___|| |_ _   _ / _|/ _|
--  _  | |/ _ \ / _ \ |  _ \  \___ \| __| | | | |_| |_ 
-- | |_| | (_) |  __/ | |_) |  ___) | |_| |_| |  _|  _|
--  \___/ \___/ \___| |____/  |____/ \__|\__,_|_| |_|  
--
-- ------------------------------------------------------------------------
-- Config File xmonad.hs for WM Xmonad
-- ------------------------------------------------------------------------
--

-- TODO                                                                 {{{
---------------------------------------------------------------------------
{-

 GENERAL
 
 * ...

 NON XMONAD SPECIFIC

 *  ... 
 
 ACTIVE

 * ... 

 DONE 

 * ... 

 TESTED/REJECTED/WONTFIX

 * ...

-}

------------------------------------------------------------------------}}}

-- IMPORT MODULES                                                       {{{
---------------------------------------------------------------------------

-- ---------------------------------
-- Import of Modules a la distrotube
-- ---------------------------------

    -- Base
import XMonad
import System.IO (hPutStrLn)
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W

import XMonad.Config.Desktop
import XMonad.Wallpaper

    -- Actions
import XMonad.Actions.CopyWindow (kill1, killAllOtherCopies)
import XMonad.Actions.CycleWS (moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import qualified XMonad.Actions.TreeSelect as TS
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import qualified XMonad.Actions.Search as S
import XMonad.Actions.UpdatePointer

    -- Data
import Data.Char (isSpace)
import Data.Monoid
import Data.Maybe (isJust)
import Data.Tree
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory
import XMonad.Hooks.DynamicBars

    -- Layouts
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.ShowWName
import XMonad.Layout.Spacing
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

    -- Prompt
import XMonad.Prompt
import XMonad.Prompt.Input
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.Man
import XMonad.Prompt.Pass
import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Prompt.Ssh
import XMonad.Prompt.XMonad
import Control.Arrow (first)

   -- Utilities
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad
-- import XMonad.Util.Run
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce

-- ---------------------------------
-- Import of Modules a la distrotube
-- ---------------------------------
------------------------------------------------------------------------}}}

-- VARIABLES / CONFIGS                                                  {{{
---------------------------------------------------------------------------

-- ---------------------------------
-- Variables / Configs
-- ---------------------------------

myFont :: String
myFont = "xft:MesloLGS NF:weight=bold:pixelsize=16:antialias=true:hinting=true"

altMask :: KeyMask
altMask = mod1Mask         -- ALT - Setting this for use in xprompts

myModMask :: KeyMask
myModMask = mod4Mask       -- Sets modkey to super/windows key

myTerminal :: String
myTerminal = "urxvt"   -- Sets default terminal
-- myTerminal = "gnome-terminal"   -- Sets default terminal

myBrowser :: String
myBrowser = "firefox "               -- Sets Firefox as browser for tree select
-- myBrowser = myTerminal ++ " -e lynx" -- Sets lynx as browser for tree select
-- myBrowser =  "xterm -e w3m " -- hets lynx as browser for tree select

myEditor :: String
-- myEditor = "emacsclient -c -a emacs "  -- Sets emacs as editor for tree select
myEditor = myTerminal ++ " -e vim "    -- Sets vim as editor for tree select

myBorderWidth :: Dimension
myBorderWidth = 3          -- Sets border width for windows

myNormColor :: String
myNormColor   = "#FFFFFF"  -- Border color of normal windows
-- solarized: myNormColor   = "#657b83"  -- Border color of normal windows
-- myNormColor   = "#282c34"  -- Border color of normal windows

myFocusColor :: String
myFocusColor  = "#9AEDFE"  -- Border color of focused windows
-- solarized: myFocusColor  = "#cb4b16"  -- Border color of focused windows
--myFocusColor  = "#bbc5ff"  -- Border color of focused windows


-- ---------------------------------
-- Variables / Configs
-- ---------------------------------
------------------------------------------------------------------------}}}

-- STARTS                                                               {{{
---------------------------------------------------------------------------

-- ---------------------------------
-- Initial Setup and Starts
-- ---------------------------------

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myStartupHook :: X ()
myStartupHook = do
          -- spawnOnce "~/.screenlayout/3-monitor-layout.sh"
          -- spawnOnce "~/.screenlayout/1-monitor-layout.sh"
          spawnOnce "xrdb ~/.Xresources"
          -- spawnOnce "nitrogen --restore &"
          -- spawnOnce "picom &"
          spawnOnce "xset r rate 300 60"
          spawnOnce "compton &"
          spawnOnce "nm-applet &"
          spawnOnce "volumeicon &"
          spawnOnce "trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 --tint 0x282c34  --height 22 &"
          -- spawnOnce "/usr/bin/emacs --daemon &"
          -- spawnOnce "kak -d -s mysession &"
          setWMName "LG3D"
          -- spawnOnce "mintUpdate &"
          -- spawnOnce "stacer"

------------------------------------------------------------------------}}}

-- GRIDS                                                                {{{
---------------------------------------------------------------------------
-- ---------------------------------
-- Grid - not really JoeB Style
-- ---------------------------------

-- Colors for gridSelect #073642 #eee8d5 #657b83 #cb4b16
myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                  (0x65,0x7b,0x83)-- lowest inactive bg
                  (0x65,0x7b,0x83)-- highest inactive bg
                  -- (0xc7,0x92,0xea) -- active bg
                  (0xcb,0x4b,0x16) -- active bg
                  --(0x65,0x7b,0x83) -- active bg
                  (0xc0,0xa7,0x9a) -- inactive fg
                  (0xee,0xe8,0xd5) -- active fg

-- gridSelect menu layout
mygridConfig :: p -> GSConfig Window
mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
    { gs_cellheight   = 40
    , gs_cellwidth    = 400
    , gs_cellpadding  = 6
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
    , gs_font         = myFont
    }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def
                   { gs_cellheight   = 40
                   , gs_cellwidth    = 200
                   , gs_cellpadding  = 6
                   , gs_originFractX = 0.5
                   , gs_originFractY = 0.5
                   , gs_font         = myFont
                   }

myAppGrid = [ ("VLC", "vlc")
                 -- ("Audacity", "audacity")
                 -- , ("Deadbeef", "deadbeef")
                 -- , ("Emacs", "emacsclient -c -a emacs")
                 , ("Firefox", "firefox")
                 , ("Geany", "geany")
                 -- , ("Geary", "geary")
                 , ("Gimp", "gimp")
                 -- , ("Kdenlive", "kdenlive")
                 , ("LibreOffice Impress", "loimpress")
                 , ("LibreOffice Writer", "lowriter")
                 , ("OBS", "obs")
                 , ("PCManFM", "pcmanfm")
                 ]

-- ---------------------------------
-- Grid - not really JoeB Style
-- ---------------------------------
------------------------------------------------------------------------}}}

-- PROMPT CONFIG                                                        {{{
---------------------------------------------------------------------------

dtXPConfig :: XPConfig
dtXPConfig = def
      { font                = myFont
      -- , bgColor             = "#282c34"
      -- , bgColor             = "#073642"
      , bgColor             = "#282A36"
      -- , fgColor             = "#bbc2cf"
      , fgColor             = "#F8F8F2"
      -- , bgHLight            = "#c792ea"
      -- , bgHLight            = "#eee8d5"
      , bgHLight            = "#eee8d5"
      -- , fgHLight            = "#000000"
      , fgHLight            = "#000000"
      , borderColor         = "#535974"
      , promptBorderWidth   = 0
      , promptKeymap        = dtXPKeymap
      , position            = Top
--    , position            = CenteredAt { xpCenterY = 0.3, xpWidth = 0.3 }
      , height              = 20
      , historySize         = 256
      , historyFilter       = id
      , defaultText         = []
      , autoComplete        = Just 100000  -- set Just 100000 for .1 sec
      , showCompletionOnTab = False
      -- , searchPredicate     = isPrefixOf
      , searchPredicate     = fuzzyMatch
      , alwaysHighlight     = True
      , maxComplRows        = Nothing      -- set to Just 5 for 5 rows
      }

-- The same config above minus the autocomplete feature which is annoying
-- on certain Xprompts, like the search engine prompts.
dtXPConfig' :: XPConfig
dtXPConfig' = dtXPConfig
      { autoComplete        = Nothing
      }

-- A list of all of the standard Xmonad prompts and a key press assigned to them.
-- These are used in conjunction with keybinding I set later in the config.
promptList :: [(String, XPConfig -> X ())]
promptList = [ ("m", manPrompt)          -- manpages prompt
             , ("p", passPrompt)         -- get passwords (requires 'pass')
             , ("g", passGeneratePrompt) -- generate passwords (requires 'pass')
             , ("r", passRemovePrompt)   -- remove passwords (requires 'pass')
             , ("s", sshPrompt)          -- ssh prompt
             , ("x", xmonadPrompt)       -- xmonad prompt
             ]

-- Same as the above list except this is for my custom prompts.
promptList' :: [(String, XPConfig -> String -> X (), String)]
promptList' = [ ("c", calcPrompt, "qalc")         -- requires qalculate-gtk
              ]

calcPrompt c ans =
    inputPrompt c (trim ans) ?+ \input ->
        liftIO(runProcessWithInput "qalc" [input] "") >>= calcPrompt c
    where
        trim  = f . f
            where f = reverse . dropWhile isSpace

dtXPKeymap :: M.Map (KeyMask,KeySym) (XP ())
dtXPKeymap = M.fromList $
     map (first $ (,) controlMask)   -- control + <key>
     [ (xK_z, killBefore)            -- kill line backwards
     , (xK_k, killAfter)             -- kill line forwards
     , (xK_a, startOfLine)           -- move to the beginning of the line
     , (xK_e, endOfLine)             -- move to the end of the line
     , (xK_m, deleteString Next)     -- delete a character foward
     , (xK_b, moveCursor Prev)       -- move cursor forward
     , (xK_f, moveCursor Next)       -- move cursor backward
     , (xK_BackSpace, killWord Prev) -- kill the previous word
     , (xK_y, pasteString)           -- paste a string
     , (xK_g, quit)                  -- quit out of prompt
     , (xK_bracketleft, quit)
     ]
     ++
     map (first $ (,) altMask)       -- meta key + <key>
     [ (xK_BackSpace, killWord Prev) -- kill the prev word
     , (xK_f, moveWord Next)         -- move a word forward
     , (xK_b, moveWord Prev)         -- move a word backward
     , (xK_d, killWord Next)         -- kill the next word
     , (xK_n, moveHistory W.focusUp')   -- move up thru history
     , (xK_p, moveHistory W.focusDown') -- move down thru history
     ]
     ++
     map (first $ (,) 0) -- <key>
     [ (xK_Return, setSuccess True >> setDone True)
     , (xK_KP_Enter, setSuccess True >> setDone True)
     , (xK_BackSpace, deleteString Prev)
     , (xK_Delete, deleteString Next)
     , (xK_Left, moveCursor Prev)
     , (xK_Right, moveCursor Next)
     , (xK_Home, startOfLine)
     , (xK_End, endOfLine)
     , (xK_Down, moveHistory W.focusUp')
     , (xK_Up, moveHistory W.focusDown')
     , (xK_Escape, quit)
     ]

archwiki, ebay, news, reddit, urban :: S.SearchEngine

archwiki = S.searchEngine "archwiki" "https://wiki.archlinux.org/index.php?search="
ebay     = S.searchEngine "ebay" "https://www.ebay.com/sch/i.html?_nkw="
news     = S.searchEngine "news" "https://news.google.com/search?q="
reddit   = S.searchEngine "reddit" "https://www.reddit.com/search/?q="
urban    = S.searchEngine "urban" "https://www.urbandictionary.com/define.php?term="

-- This is the list of search engines that I want to use. Some are from
-- XMonad.Actions.Search, and some are the ones that I added above.
searchList :: [(String, S.SearchEngine)]
searchList = [ ("a", archwiki)
             , ("d", S.duckduckgo)
             , ("e", ebay)
             , ("g", S.google)
             , ("h", S.hoogle)
             , ("i", S.images)
             , ("n", news)
             , ("r", reddit)
             , ("s", S.stackage)
             , ("t", S.thesaurus)
             , ("v", S.vocabulary)
             , ("b", S.wayback)
             , ("u", urban)
             , ("w", S.wikipedia)
             , ("y", S.youtube)
             , ("z", S.amazon)
             ]

------------------------------------------------------------------------}}}

-- myScratchPads - for terminal, ranger and other stuff                 {{{
---------------------------------------------------------------------------

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "ranger" spawnRanger findRanger manageRanger
                , NS "mocp" spawnMocp findMocp manageMocp
                ]
  where
    -- spawnTerm  = myTerminal ++ " -n scratchpad"
    -- spawnTerm  = "xterm -name scratchpad"
    spawnTerm  = "urxvt -name scratchpad"
    findTerm   = resource =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnRanger  = "xterm -name rangerpad -e ranger"
    findRanger   = resource =? "rangerpad"
    manageRanger = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnMocp  = myTerminal ++ " -n mocp 'mocp'"
    findMocp   = resource =? "mocp"
    manageMocp = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w

------------------------------------------------------------------------}}}

-- LAYOUTS                                                              {{{
---------------------------------------------------------------------------

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
tall     = renamed [Replace "tall"]
           $ limitWindows 12
           $ mySpacing 4
           $ ResizableTall 1 (3/100) (1/2) []
magnify  = renamed [Replace "magnify"]
           $ magnifier
           $ limitWindows 12
           $ mySpacing 4
           $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ limitWindows 12
           $ mySpacing 4
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ mySpacing' 4
           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ limitWindows 7
           $ mySpacing' 4
           $ ThreeCol 1 (3/100) (1/2)
threeRow = renamed [Replace "threeRow"]
           $ limitWindows 7
           $ mySpacing' 4
           -- Mirror takes a layout and rotates it by 90 degrees.
           -- So we are applying Mirror to the ThreeCol layout.
           $ Mirror
           $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           -- I cannot add spacing to this layout because it will
           -- add spacing between window and tabs which looks bad.
           $ tabbed shrinkText myTabConfig
  where
    myTabConfig = def { fontName            = "xft:MesloLGS NF:regular:pixelsize=14"
                      , activeColor         = "#BD93F9"
                      -- solarized , activeColor         = "#cb4b16"
                      -- , activeColor         = "#282c34"
                      , inactiveColor       = "#8BE9FD"
                      -- solarized , inactiveColor       = "#859900"
                      -- , inactiveColor       = "#3e445e"
                      , activeBorderColor   = "#282c34"
                      , inactiveBorderColor = "#282c34"
                      , activeTextColor     = "#ffffff"
                      , inactiveTextColor   = "#000000"
                      }


-- Theme for showWName which prints current workspace when you change workspaces.
    -- { swn_font              = "xft:Sans:bold:size=60"
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:MesloLGS NF:bold:pixelsize=72"
    , swn_fade              = 2.0
    -- , swn_bgcolor           = "#cb4b16"
    , swn_bgcolor           = "#BD93F9"
    , swn_color             = "#FFFFFF"
    }


-- The layout hook
myLayoutHook = avoidStruts $ showWName' myShowWNameTheme $ mouseResize $ windowArrange $ T.toggleLayouts floats $
               mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               -- I've commented out the layouts I don't use.
               myDefaultLayout =     tall
                                 ||| magnify
                                 ||| noBorders monocle
                                 ||| floats
                                 ||| grid
                                 ||| noBorders tabs
                                 ||| spirals
                                 ||| threeCol
                                 ||| threeRow

xmobarEscape :: String -> String
xmobarEscape = concatMap doubleLts
  where
        doubleLts '<' = "<<"
        doubleLts x   = [x]

{--
myWorkspaces :: [String]
myWorkspaces = clickable . (map xmobarEscape)
                $ ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
                $ ["dev", "www", "sys", "doc", "vbox", "chat", "mus", "vid", "gfx"]
                $ ["\xf120 trm", "\xf269 www", "\xf07c fil", "\xf121 cod", "\xf0f7 off", "\xf0ec ftp", "\xf1c1 pdf", "\xf001 mms", "\xf0c2 vms"]
                $ ["\xf17a trm", "\xf13b www", "\xf07c fil", "\xf1c9 cod", "\xf1fe off", "\xf1e0 ftp", "\xf1c1 pdf", "\xf008 mms", "\xf26c vms"]
                $ [" \xf17a ", " \xf13b ", " \xf07c ", " \xf1c9 ", " \xf1fe ", " \xf1e0 ", " \xf1c1 ", " \xf008 ", " \xf26c "]
                $ ["\xf120", "\xf269", "\xf07c", "\xf121", "\xf0f7", "\xf0ec", "\xf1c1", "\xf001", "\xf0c2"]
  where
        clickable l = [ "<action=xdotool key super+" ++ show (n) ++ ">" ++ ws ++ "</action>" |
                      (i,ws) <- zip [1..9] l,
                      let n = i ]
--}

myWorkspaces :: [String]
myWorkspaces = clickable . (map xmobarEscape)
               -- $ ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
               -- $ ["dev", "www", "sys", "doc", "vbox", "chat", "mus", "vid", "gfx"]
               -- $ ["\xf120 trm", "\xf269 www", "\xf07c fil", "\xf121 cod", "\xf0f7 off", "\xf0ec ftp", "\xf1c1 pdf", "\xf001 mms", "\xf0c2 vms"]
               -- $ ["\xf17a trm", "\xf13b www", "\xf07c fil", "\xf1c9 cod", "\xf1fe off", "\xf1e0 ftp", "\xf1c1 pdf", "\xf008 mms", "\xf26c vms"]
               $ ["\xf17a ", "\xf13b ", "\xf07c ", "\xf1c9 ", "\xf1fe ", "\xf1e0 ", "\xf1c1 ", "\xf008 ", "\xf26c "]
               -- $ ["\xf120", "\xf269", "\xf07c", "\xf121", "\xf0f7", "\xf0ec", "\xf1c1", "\xf001", "\xf0c2"]
  where
        clickable l = [ " " ++ show (n) ++ " " ++ ws |
                      (i,ws) <- zip [1..9] l,
                      let n = i ]


myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     -- I'm doing it this way because otherwise I would have to write out
     -- the full name of my workspaces.
     [ className =?     "htop"                                  --> doShift ( myWorkspaces !! 7 )
     -- , title =?         "firefox"                       --> doShift ( myWorkspaces !! 1 )
     -- , (className =?    "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
     , className =?     "Firefox"                               --> doShift ( myWorkspaces !! 1 )
     , (className =?    "Firefox" <&&> resource =? "Dialog")    --> doFloat  -- Float Firefox Dialog
     , (className =?    "Firefox" <&&> title =? "Über Mozilla Firefox")    --> doCenterFloat  -- Float Firefox Dialog
     , className =?     "Yad"                                   --> doCenterFloat  -- Float Firefox Dialog
     , title =?         "qutebrowser"                           --> doShift ( myWorkspaces !! 1 )
     , className =?     "mpv"                                   --> doShift ( myWorkspaces !! 7 )
     -- , className =? "vlc"     --> doShift ( myWorkspaces !! 7 )
     , className =?     "Gimp"                                  --> doShift ( myWorkspaces !! 7 )
     --, className =?     "Gimp"                          --> doFloat
     , className =?     "libreoffice-startcenter"               --> doShift ( myWorkspaces !! 4 )
     -- , title =?         "Oracle VM VirtualBox Manager"  --> doFloat
     -- , className =?     "VirtualBox Machine"            --> doFloat
     , className =?     "VirtualBox Manager"                    --> doShift ( myWorkspaces !! 8 )
     , title =?         "Persönlicher Ordner"                   --> doShift ( myWorkspaces !! 2 )
     , className =?     "nemo"                                  --> doShift ( myWorkspaces !! 2 )
     , className =?     "p3x-onenote"                           --> doShift ( myWorkspaces !! 3 )
     ] <+> namedScratchpadManageHook myScratchPads

myLogHook :: X ()
myLogHook = fadeInactiveLogHook fadeAmount
    where fadeAmount = 0.8
    -- where fadeAmount = 1.0


------------------------------------------------------------------------}}}

-- SHORTCUTS - myKeys                                                   {{{
---------------------------------------------------------------------------

myKeys :: [(String, X ())]
myKeys =
--START_KEYS
    -- KB_GROUP Xmonad
        [ ("M-C-r", spawn "xmonad --recompile")      -- Recompiles xmonad
        , ("M-S-r", spawn "xmonad --restart")        -- Restarts xmonad
        , ("M-S-q", io exitSuccess)                  -- Quits xmonad

    -- KB_GROUP Open my preferred terminal
        , ("M-<Return>", spawn myTerminal)
        , ("M1-t", spawn "urxvt -e tmux")
        -- , ("M-M1-<Return>", spawn "urxvt")
        , ("M-M1-<Return>", spawn "st")
        , ("M-r", spawn "xterm -e ranger")
        -- , ("M-<Return>", spawn (myTerminal ++ " -e fish"))

    -- KB_GROUP Run Prompt
        , ("M-S-<Return>", shellPrompt dtXPConfig)   -- Shell Prompt
    -- special Help with dmenu, rofi and zathura
        , ("M-M1-m", spawn "man2pdf.sh")
        , ("M-M1-y", spawn "youtubeforward.sh")
    -- KB_GROUP Windows
        , ("M-S-c", kill1)                           -- Kill the currently focused client
        , ("M-S-a", killAll)                         -- Kill all windows on current workspace

    -- KB_GROUP Grid
    -- Grid Select (CTRL-g followed by a key)
        , ("C-g g", spawnSelected' myAppGrid)                 -- grid select favorite apps
        -- , ("C-M1-g", withSelectedWindow $ mygridConfig myColorizer)                -- grid testing Joe B
        -- , ("C-g t", goToSelected $ mygridConfig myColorizer)  -- goto selected window
        , ("M1-<Tab>", goToSelected $ mygridConfig myColorizer)  -- goto selected window
        , ("C-g b", bringSelected $ mygridConfig myColorizer) -- bring selected window

    -- KB_GROUP Floating windows
        , ("M-f", sendMessage (T.Toggle "floats"))       -- Toggles my 'floats' layout
        , ("M-<Delete>", withFocused $ windows . W.sink) -- Push floating window back to tile
        , ("M-S-<Delete>", sinkAll)                      -- Push ALL floating windows to tile
        --, ("M-S-u", sinkAll)                      -- Push ALL floating windows to tile

    -- KB_GROUP Windows navigation
        , ("M-m", windows W.focusMaster)     -- Move focus to the master window
        , ("M-j", windows W.focusDown)       -- Move focus to the next window
        , ("M-k", windows W.focusUp)         -- Move focus to the prev window
        --, ("M-S-m", windows W.swapMaster)    -- Swap the focused window and the master window
        , ("M-S-j", windows W.swapDown)      -- Swap focused window with next window
        , ("M-S-k", windows W.swapUp)        -- Swap focused window with prev window
        , ("M-<Backspace>", promote)         -- Moves focused window to master, others maintain order
        , ("M1-S-<Tab>", rotSlavesDown)      -- Rotate all windows except master and keep focus in place
        , ("M1-C-<Tab>", rotAllDown)         -- Rotate all the windows in the current stack
        --, ("M-S-s", windows copyToAll)
        , ("M-C-s", killAllOtherCopies)

        -- Layouts
        , ("M-<Tab>", sendMessage NextLayout)                -- Switch to next layout
        , ("M-C-M1-<Up>", sendMessage Arrange)
        , ("M-C-M1-<Down>", sendMessage DeArrange)
        , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
        , ("M-S-<Space>", sendMessage ToggleStruts)         -- Toggles struts
        , ("M-S-n", sendMessage $ MT.Toggle NOBORDERS)      -- Toggles noborder
        --, ("M-<KP_Multiply>", sendMessage (IncMasterN 1))   -- Increase number of clients in master pane
        --, ("M-<KP_Divide>", sendMessage (IncMasterN (-1)))  -- Decrease number of clients in master pane
        , ("M-,", sendMessage (IncMasterN 1))   -- Increase number of clients in master pane
        , ("M-M1-,", sendMessage (IncMasterN (-1)))  -- Decrease number of clients in master pane
        , ("M-S-<KP_Multiply>", increaseLimit)              -- Increase number of windows
        , ("M-S-<KP_Divide>", decreaseLimit)                -- Decrease number of windows

        , ("M-h", sendMessage Shrink)                       -- Shrink horiz window width
        , ("M-l", sendMessage Expand)                       -- Expand horiz window width
        , ("M-C-j", sendMessage MirrorShrink)               -- Shrink vert window width
        , ("M-C-k", sendMessage MirrorExpand)               -- Exoand vert window width

    -- KB_GROUP Workspaces
        -- , ("M-.", nextScreen)  -- Switch focus to next monitor
        -- , ("M-.", moveTo Next nonNSP)  -- go to to next monitor
        , ("C-M-<Right>", moveTo Next nonNSP)  -- go to to next monitor
        -- , ("M-,", moveTo Prev nonNSP)  -- go to to prev monitor
        , ("C-M-<Left>", moveTo Prev nonNSP)  -- go to to prev monitor
        -- , ("M-,", prevScreen)  -- Switch focus to prev monitor
        , ("M-S-<KP_Add>", shiftTo Next nonNSP >> moveTo Next nonNSP)       -- Shifts focused window to next ws
        , ("M-S-<KP_Subtract>", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to prev ws

    -- KB_GROUP Scratchpads
        , ("M-C-<Return>", namedScratchpadAction myScratchPads "terminal")
        , ("M-M1-r", namedScratchpadAction myScratchPads "ranger")
        , ("M-C-c", namedScratchpadAction myScratchPads "mocp")

    -- Controls for mocp music player.
        , ("M-u p", spawn "mocp --play")
        , ("M-u l", spawn "mocp --next")
        , ("M-u h", spawn "mocp --previous")
        , ("M-u <Space>", spawn "mocp --toggle-pause")

{-
    -- Emacs (CTRL-e followed by a key)
        , ("C-e e", spawn "emacsclient -c -a 'emacs'")                            -- start emacs
        , ("C-e b", spawn "emacsclient -c -a 'emacs' --eval '(ibuffer)'")         -- list emacs buffers
        , ("C-e d", spawn "emacsclient -c -a 'emacs' --eval '(dired nil)'")       -- dired emacs file manager
        , ("C-e i", spawn "emacsclient -c -a 'emacs' --eval '(erc)'")             -- erc emacs irc client
        , ("C-e m", spawn "emacsclient -c -a 'emacs' --eval '(mu4e)'")            -- mu4e emacs email client
        , ("C-e n", spawn "emacsclient -c -a 'emacs' --eval '(elfeed)'")          -- elfeed emacs rss client
        , ("C-e s", spawn "emacsclient -c -a 'emacs' --eval '(eshell)'")          -- eshell within emacs
        , ("C-e t", spawn "emacsclient -c -a 'emacs' --eval '(mastodon)'")        -- mastodon within emacs
        , ("C-e v", spawn "emacsclient -c -a 'emacs' --eval '(+vterm/here nil)'") -- vterm within emacs
        -- emms is an emacs audio player. I set it to auto start playing in a specific directory.
        , ("C-e a", spawn "emacsclient -c -a 'emacs' --eval '(emms)' --eval '(emms-play-directory-tree \"~/Music/Non-Classical/70s-80s/\")'")
-}

--- KB_GROUP My Applications (Super+Alt+Key)
        , ("M-M1-a", spawn (myTerminal ++ " -e alsamixer"))
        , ("M-M1-v", spawn (myTerminal ++ " -e nvim"))
        , ("M-M1-b", spawn "qutebrowser www.youtube.com/c/DistroTube/")
        , ("M-M1-p", spawn "qutebrowser www.pcsystembetreuer.de")
        -- , ("M-d", spawn "rofi -lines 15 -padding 18 -width 80 -location 0 -show drun -sidebar-mode -columns 3 -font 'MesloLGS NF 10' -show-icons -theme $HOME/.config/rofi/themes/dracula-alternate-joeb.rasi")
        , ("M-d", spawn "rofi -show drun")
        -- , ("M-d", spawn "rofi -lines 15 -padding 18 -width 80 -location 0 -show drun -sidebar-mode -columns 3 -font 'MesloLGS NF 10' -show-icons -theme $HOME/.config/rofi/themes/solarized-alternate-joeb.rasi")
        -- , ("M-d", spawn "rofi -lines 15 -padding 18 -width 80 -location 0 -show drun -sidebar-mode -columns 3 -font 'MesloLGS NF 10' -show-icons -theme $HOME/.config/rofi/themes/solarized-alternate-joeb.rasi")
        -- , ("M-M1-b", spawn "surf www.youtube.com/c/DistroTube/")
        -- , ("M-M1-e", spawn (myTerminal ++ " -e neomutt"))
        -- , ("M-M1-f", spawn (myTerminal ++ " -e sh ./.config/vifm/scripts/vifmrun | bash"))
        -- , ("M-M1-i", spawn (myTerminal ++ " -e irssi"))
        -- , ("M-M1-j", spawn (myTerminal ++ " -e joplin"))
        -- , ("M-M1-l", spawn (myTerminal ++ " -e lynx https://distrotube.com"))
        -- , ("M-M1-m", spawn (myTerminal ++ " -e mocp"))
        -- , ("M-M1-n", spawn (myTerminal ++ " -e newsboat"))
        -- , ("M-M1-p", spawn (myTerminal ++ " -e pianobar"))
        -- , ("M-M1-r", spawn (myTerminal ++ " -e rtv"))
        -- , ("M-M1-t", spawn (myTerminal ++ " -e toot curses"))
        -- , ("M-M1-w", spawn (myTerminal ++ " -e wopr report.xml"))
        -- , ("M-M1-y", spawn (myTerminal ++ " -e youtube-viewer"))
        , ("M-M1-c", spawn "gsimplecal")
        , ("M-w", spawn myBrowser )
        , ("M1-v", spawn "vieb" )
        , ("M-e", spawn "nemo")
        , ("C-M1-l", spawn "i3lock-fancy")
        --, ("M-M1-l", spawn (myTerminal ++ " -e jbsuspend"))
        --, ("<F1>", spawn "~/.xmonad/scripts/xmonad-shortcuts-4-dzen.sh")
        , ("<F1>", spawn "~/.xmonad/scripts/xmonad_keys.sh")

    -- Multimedia Keys
        , ("<XF86AudioPlay>", spawn "cmus toggle")
        , ("<XF86AudioPrev>", spawn "cmus prev")
        , ("<XF86AudioNext>", spawn "cmus next")
        -- , ("<XF86AudioMute>",   spawn "amixer set Master toggle")  -- Bug prevents it from toggling correctly in 12.04.
        , ("<XF86AudioMute>",   spawn "amixer -q -D pulse sset Master toggle")        
        , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
        , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
        , ("<XF86HomePage>", spawn "firefox")
        , ("<XF86Search>", safeSpawn "firefox" ["https://www.google.com/"])
        , ("<XF86Mail>", runOrRaise "geary" (resource =? "thunderbird"))
        , ("<XF86Calculator>", runOrRaise "gcalctool" (resource =? "gcalctool"))
        , ("<XF86Eject>", spawn "toggleeject")
        -- , ("<Print>", spawn "scrotd 0")
        , ("<Print>", spawn "scrot 'cheese_%a-%d%b%y_%H.%M.png' -e 'viewnior ~/$f'")
        ]
        -- Appending search engine prompts to keybindings list.
        -- Look at "search engines" section of this config for values for "k".
        ++ [("M-s " ++ k, S.promptSearch dtXPConfig' f) | (k,f) <- searchList ]
        ++ [("M-S-s " ++ k, S.selectSearch f) | (k,f) <- searchList ]
        -- Appending some extra xprompts to keybindings list.
        -- Look at "xprompt settings" section this of config for values for "k".
        ++ [("M-p " ++ k, f dtXPConfig') | (k,f) <- promptList ]
        ++ [("M-p " ++ k, f dtXPConfig' g) | (k,f,g) <- promptList' ]
        -- The following lines are needed for named scratchpads.
          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "nsp"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "nsp"))

--END_KEYS
------------------------------------------------------------------------}}}

-- main                                                                 {{{
---------------------------------------------------------------------------

main :: IO ()
main = do
    -- Launching three instances of xmobar on their monitors.
    xmproc <- spawnPipe "xmobar /home/joeb/.config/xmobar/xmobarrc"
    -- xmprocbottom <- spawnPipe "xmobar /home/joeb/.config/xmobar/xmobarrc-bottom"
    -- xmproc0 <- spawnPipe "xmobar /home/joeb/.config/xmobar/xmobarrc0"
    -- xmproc1 <- spawnPipe "xmobar /home/joeb/.config/xmobar/xmobarrc1"
    -- xmproc2 <- spawnPipe "xmobar /home/joeb/.config/xmobar/xmobarrc2"
    -- xmproc2 <- spawnPipe "xmobar -x 0 /home/joeb/.config/xmobar/xmobarrc2"
    setRandomWallpaper ["/home/joeb/Bilder/wallpaper/set12-draculacolored"]
    -- the xmonad, ya know...what the WM is named after!
    xmonad $ ewmh def
        { manageHook = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageDocks
        -- Run xmonad commands from command line with "xmonadctl command". Commands include:
        -- shrink, expand, next-layout, default-layout, restart-wm, xterm, kill, refresh, run,
        -- focus-up, focus-down, swap-up, swap-down, swap-master, sink, quit-wm. You can run
        -- "xmonadctl 0" to generate full list of commands written to ~/.xsession-errors.
        , handleEventHook    = serverModeEventHookCmd
                               <+> serverModeEventHook
                               <+> serverModeEventHookF "XMONAD_PRINT" (io . putStrLn)
                               <+> docksEventHook
        , modMask            = myModMask
        , terminal           = myTerminal
        , startupHook        = myStartupHook
        , layoutHook         = myLayoutHook
        , workspaces         = myWorkspaces
        , borderWidth        = myBorderWidth
        , normalBorderColor  = myNormColor
        , focusedBorderColor = myFocusColor
        , logHook = workspaceHistoryHook <+> myLogHook <+> dynamicLogWithPP xmobarPP
                        -- { ppOutput = \x -> hPutStrLn xmproc2 x  >> hPutStrLn xmproc1 x  >> hPutStrLn xmproc0 x
                        { ppOutput =  hPutStrLn xmproc
                        -- , ppCurrent = xmobarColor "#98be65" "" . wrap "[" "]" -- Current workspace in xmobar
                        , ppCurrent = xmobarColor "#BD93F9" "" . wrap "[" "]" -- Current workspace in xmobar
                        -- , ppVisible = xmobarColor "#98be65" ""                -- Visible but not current workspace
                        , ppVisible = xmobarColor "#F8F8F2" ""                -- Visible but not current workspace
                        , ppHidden = xmobarColor "#5AF78E" "" . wrap "*" ""   -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor "#F8F8F2" ""        -- Hidden workspaces (no windows)
                        , ppTitle = xmobarColor "#BD93F9" "" . shorten 60     -- Title of active window in xmobar
                        , ppSep =  "<fc=#F8F8F2> <fn=2>|</fn> </fc>"                     -- Separators in xmobar
                        , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
                        , ppExtras  = [windowCount]                           -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                        }
                    >> updatePointer (0.5, 0.5) (0, 0)
        } `additionalKeysP` myKeys

------------------------------------------------------------------------}}}

-- 
-- vim: ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
