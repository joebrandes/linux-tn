!special hint - look below - correct term name
!URxvt*termName:			rxvt-unicode-256color

!correct coloring with hack_color for color8
#define hack_color      #c1c1c1

#define S_base03        #002b36
#define S_base02        #073642
#define S_base01        #586e75
#define S_base00        #657b83
#define S_base0         #839496
#define S_base1         #93a1a1
#define S_base2         #eee8d5
#define S_base3         #fdf6e3

!*background:            S_base03
*background:            S_base03
*foreground:            S_base0
*fadeColor:             S_base03
*cursorColor:           S_base1
*pointerColorBackground:S_base01
*pointerColorForeground:S_base1

#define S_yellow        #b58900
#define S_orange        #cb4b16
#define S_red           #dc322f
#define S_magenta       #d33682
#define S_violet        #6c71c4
#define S_blue          #268bd2
#define S_cyan          #2aa198
#define S_green         #859900

!! black dark/light
*color0:                S_base02
!*color8:                S_base03
*color8:                hack_color
XTerm*color8:           S_base03
!! red dark/light
*color1:                S_red
*color9:                S_orange

!! green dark/light
*color2:                S_green
*color10:               S_base01

!! yellow dark/light
*color3:                S_yellow
*color11:               S_base00

!! blue dark/light
*color4:                S_blue
*color12:               S_base0

!! magenta dark/light
*color5:                S_magenta
*color13:               S_violet

!! cyan dark/light
*color6:                S_cyan
*color14:               S_base1

!! white dark/light
*color7:                S_base2
*color15:               S_base3


! ------------------------------------------------------------------------------
! XTerm Font configuration
! ------------------------------------------------------------------------------

XTerm*font:      xft:MesloLGS NF:size=12
XTerm*boldFont:  xft:MesloLGS NF:bold:size=12
XTerm*renderFont: true
!XTerm*faceName: xft:MesloLGS NF
!                xft:JoyPixels:size=12, \
!                xft:Monospace:style=Medium:size=12   
!XTerm*faceSize: 11
XTerm*utf8: 1
XTerm*locale: true

XTerm.vt100.translations: #override \n\
  Ctrl <Key> j: smaller-vt-font() \n\
  Ctrl <Key> k: larger-vt-font()


! Every shell is a login shell by default (for inclusion of all necessary environment variables)
XTerm*loginshell: true

! I like a LOT of scrollback...
XTerm*savelines: 16384

! double-click to select whole URLs :D
XTerm*charClass: 33:48,36-47:48,58-59:48,61:48,63-64:48,95:48,126:48

! ------------------------------------------------------------------------------
! Urxvt Font configuration
! ------------------------------------------------------------------------------

URxvt*font:      xft:MesloLGS NF:size=12
URxvt*boldFont:  xft:MesloLGS NF:bold:size=12
!URxvt*boldFont:			xft:M+ 1mn:bold:size=8
!URxvt*italicFont:		xft:M+ 1mn:italic:size=8
!URxvt*boldItalicFont:	xft:M+ 1mn:bold italic:size=8


! ------------------------------------------------------------------------------
! Xft Font Configuration
! ------------------------------------------------------------------------------

Xft.autohint: 0
Xft.lcdfilter: lcddefault
Xft.hintstyle: hintslight
Xft.hinting: 1
Xft.antialias: 1
Xft.rgba: rgb


! ------------------------------------------------------------------------------
! URxvt configs
! ------------------------------------------------------------------------------

! font spacing
URxvt*letterSpace:		0
URxvt.lineSpace:		0

! general settings
URxvt*saveline:			15000
URxvt*termName:			rxvt-unicode-256color
URxvt*iso14755:			false
URxvt*urgentOnBell:		true

! appearance
URxvt*depth:			24
URxvt*scrollBar:		false
URxvt*scrollBar_right:	false
URxvt*internalBorder:	24
URxvt*externalBorder:	0
URxvt.geometry:			84x22


! perl extensions
URxvt.perl-ext-common:	default,clipboard,url-select,keyboard-select

! macros for clipboard and selection
URxvt.copyCommand:		xclip -i -selection clipboard
URxvt.pasteCommand:		xclip -o -selection clipboard
URxvt.keysym.M-c:		perl:clipboard:copy
URxvt.keysym.M-v:		perl:clipboard:paste
URxvt.keysym.M-C-v:		perl:clipboard:paste_escaped
URxvt.keysym.M-Escape:	perl:keyboard-select:activate
URxvt.keysym.M-s:		perl:keyboard-select:search
URxvt.keysym.M-u:		perl:url-select:select_next
URxvt.urlLauncher:		firefox
URxvt.underlineURLs:	true
URxvt.urlButton:		1     

! scroll one line
URxvt.keysym.Shift-Up:		command:\033]720;1\007
URxvt.keysym.Shift-Down:	command:\033]721;1\007

! control arrow
URxvt.keysym.Control-Up:	\033[1;5A
URxvt.keysym.Control-Down:	\033[1;5B
URxvt.keysym.Control-Right:	\033[1;5C
URxvt.keysym.Control-Left:	\033[1;5D


! ------------------------------------------------------------------------------
! Rofi configs
! ------------------------------------------------------------------------------

!rofi.color-enabled: true
!rofi.color-window: #2e3440, #2e3440, #2e3440
!rofi.color-normal: #2e3440, #d8dee9, #2e3440, #2e3440, #bf616a
!rofi.color-active: #2e3440, #b48ead, #2e3440, #2e3440, #93e5cc
!rofi.color-urgent: #2e3440, #ebcb8b, #2e3440, #2e3440, #ebcb8b
!rofi.modi: run,drun,window,ssh

! ------------------------------------------------------------------------------
! Dmenu configs
! ------------------------------------------------------------------------------

!dmenu.selforeground:	    #d8dee9
!dmenu.background:	        #2e3440
!dmenu.selbackground:	    #bf616a
!dmenu.foreground:	        #d8dee9
                                                           

