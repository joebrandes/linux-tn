# Xmonad

Window Manager always kind of interested me. But i never took *Deep Dives* in
them - so no efforts with them whatsoever.

In the last years i took the task on to build me an i3-Environment i can live on
on a daily basis.

The following stuff is for **Xmonad** (and **xmobar**) and is mostly on stuff
*DT* talked about in his **Distrotube Youtube** Channel.

In Essence: *Xmonad is the Suckless approach, that DWM tries but ...*


**IMPORTANT** - These are **no Copy & Paste solutions**

These are Code-Snippets to gradually implement in **your own Environment**! I'm not
even saying, that i'm using all the stuff.

And obviously you need the software installed using in my Config-Files.

Example Installation for Linux Mint 20:

    sudo apt install xmonad libghc-xmonad-contrib-dev xmobar

And i know with the Deployment of **Haskell** Stuff, there is a lot of data landing
in your System - roughly **1GB**! That is al lot for a simple *WM*, but we obviously get
all the Environment for **Haskell Programming** also.

And don't forget the ususal stuff for WM Environments. Without e.g. **XTerm** you maybe
get stranded and lost in the WM of your Choosing!

    sudo apt install xterm rofi dmenu dzen2 .. or whatever ..

It makes no sense to log in Xmonad without any **Environment / Configurations**! A dummy Login
into Xmonad-Session leaves you like a *dummy*. The Community has different starting points and
serves **xmonad.hs** startconfigs. I normally start with my own stuff.

And there is a ton of stuff out there to show the Xmonad Tech Specifications
and Configurations. All Xmonad Stuff is written in Haskell - there is
even a special Search-Tech called **Hoogle** ;-)

## Links / Technologies

* [Xmonad](https://xmonad.org) - Xmonad Project Website
* [Haskell Searchenginge](https://hoogle.haskell.org) - Hoogle for Haskell
* [Joe Brandes](http://www.joe-brandes.de) - Web *Card* by Joe Brandes
* [Git](https://www.git-scm.com/) - Git Development

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
