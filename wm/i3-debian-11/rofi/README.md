
# Rofi

[Github Repo - user - OneDark](https://github.com/davatorium/rofi-themes/blob/master/User%20Themes/onedark.rasi)

I sponsor a few variations in my ``.config/rofi/themes`` Folder.

Without this Themes Folder there is no functioning Rofi with Meta + D.
