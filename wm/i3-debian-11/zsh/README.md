# ZSH

I go fully ZSH on my Systems!

*   ZSH-Plugin-Partner is **Antigen**
*   Search and find: **FZF** and **Ripgrep**
*   Styling via **starship.rs**

Install the ZSH and tools: ``sudo apt install zsh fzf ripgrep python3-pygments``

Curl the Antigen: ``curl -L git.io/antigen > antigen.zsh`` - or use my Version

Get Starship.rs: ``curl -sS https://starship.rs/install.sh | sh``

Remember: for some Configurations to take effect, there is an Re-Login or
at least a sourcing of the config file neccessary!

More Configuration for Starship: https://starship.rs/config/#prompt
for file ~/.config/starship.toml.

And i have an Example in this docs.

And in the end: **Make ZSH your Login-Shell!**
