# set modifier
set $super Mod4
set $alt Mod1

# set font
font pango: Noto Sans 12

# Use Mouse+$super to drag floating windows to their wanted position
floating_modifier $super

#autostart
exec --no-startup-id ~/.screenlayout/1920x1080.sh
exec --no-startup-id nitrogen --restore &
# exec --no-startup-id hsetroot -center ~/.wallpaper.png
exec --no-startup-id xsettingsd &
# exec --no-startup-id compton -b
exec --no-startup-id picom -b

# start a terminal
bindsym $super+Return exec gnome-terminal
#bindsym $super+Return exec kitty;focus
#bindsym $super+Return exec urxvt
#bindsym $super+Return exec i3-sensible-terminal

# start dmenu (a program launcher)
bindsym $super+Shift+d exec i3-dmenu-desktop --dmenu="dmenu -i -fn 'MesloLGS NF:size=12'"
#bindsym $super+Shift+d exec i3-dmenu-desktop --dmenu="dmenu -i -fn 'Noto Sans:size=12'"
# bindsym $super+d exec rofi -lines 12 -padding 18 -width 60 -location 0 -show drun -sidebar-mode -columns 3 -font 'Noto Sans 12'
bindsym $super+d exec rofi -lines 15 -padding 28 -width 100 -show drun -sidebar-mode -columns 2 -font 'MesloLGS NF 12' -show-icons -theme '/home/joeb/.config/rofi/themes/onedark-joeb.rasi'
# bindsym $super+d exec rofi -lines 15 -padding 28 -width 100 -show drun -sidebar-mode -columns 2 -show-icons

# common apps keybinds
bindsym Print exec scrot 'Cheese_%a-%d%b%y_%H.%M.png' -e 'viewnior ~/$f'
bindsym $super+l exec i3lock-fancy
#bindsym $super+l exec i3lock -i ~/.wallpaper.png
bindsym $super+Shift+w exec firefox
bindsym $super+Shift+f exec thunar;workspace 3;focus
bindsym $super+Shift+g exec geany

#change volume
bindsym XF86AudioRaiseVolume exec amixer -q set Master 5%+
bindsym XF86AudioLowerVolume exec amixer -q set Master 5%-
bindsym XF86AudioMute exec amixer set Master toggle

# music control
bindsym XF86AudioNext exec mpc next
bindsym XF86AudioPrev exec mpc prev
bindsym XF86AudioPlay exec mpc toggle
bindsym XF86AudioStop exec mpc stop

# kill focused window
bindsym $super+c kill
bindsym $alt+F4 kill

# change focus
bindsym $super+Left focus left
bindsym $super+Down focus down
bindsym $super+Up focus up
bindsym $super+Right focus right

# move focused window
bindsym $super+Shift+Left move left
bindsym $super+Shift+Down move down
bindsym $super+Shift+Up move up
bindsym $super+Shift+Right move right

# split in horizontal orientation
bindsym $super+h split h

# split in vertical orientation
bindsym $super+v split v

# enter fullscreen mode for the focused container
bindsym $super+f fullscreen toggle

# change container layout split
bindsym $super+s layout toggle split

# toggle tiling / floating
bindsym $super+space floating toggle

# change focus between tiling / floating windows
bindsym $super+Shift+space focus mode_toggle

# Workspaces 
set $ws1 1 : Browser
set $ws2 2 : Dateien
set $ws3 3 : Konsole
set $ws4 4 : Coding
set $ws5 5 : VMS
set $ws6 6 : Office
set $ws7 7 : E-Mail
set $ws8 8 : Multimedia
set $ws9 9 : System


# switch to workspace
bindsym $alt+Control+Right workspace next
bindsym $alt+Control+Left workspace prev
bindsym $super+1 workspace $ws1
bindsym $super+2 workspace $ws2
bindsym $super+3 workspace $ws3
bindsym $super+4 workspace $ws4
bindsym $super+5 workspace $ws5
bindsym $super+6 workspace $ws6
bindsym $super+7 workspace $ws7
bindsym $super+8 workspace $ws8
bindsym $super+9 workspace $ws9

# move focused container to workspace
bindsym $super+Shift+1 move container to workspace $ws1
bindsym $super+Shift+2 move container to workspace $ws2
bindsym $super+Shift+3 move container to workspace $ws3
bindsym $super+Shift+4 move container to workspace $ws4
bindsym $super+Shift+5 move container to workspace $ws5
bindsym $super+Shift+6 move container to workspace $ws6
bindsym $super+Shift+7 move container to workspace $ws7
bindsym $super+Shift+8 move container to workspace $ws8
bindsym $super+Shift+9 move container to workspace $ws9

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $super+Shift+r restart

# exit i3
bindsym $super+q exec "i3-nagbar -t warning -m 'Really, exit?' -b 'Yes' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        bindsym Left resize shrink width 5 px or 5 ppt
        bindsym Down resize grow height 5 px or 5 ppt
        bindsym Up resize shrink height 5 px or 5 ppt
        bindsym Right resize grow width 5 px or 5 ppt
        bindsym Return mode "default"
}
bindsym $super+r mode "resize"

# panel
bar {
	position top
        colors {
        # background #2f343f
        # statusline #2f343f
        background #1E2127
        statusline #1E2127
        # separator #4b5262
        separator #ABB2BF

        # colour of border, background, and text
        #focused_workspace       #2f343f #bf616a #d8dee8
        #active_workspace        #2f343f #2f343f #d8dee8
        #inactive_workspace      #2f343f #2f343f #d8dee8
        #urgent_workspacei       #2f343f #ebcb8b #2f343f
        focused_workspace        #1E2127 #e06c75 #ffffff
        active_workspace         #1E2127 #1E2127 #ABB2BF
        inactive_workspace       #1E2127 #1E2127 #ABB2BF
        urgent_workspacei        #1E2127 #ebcb8b #2f343f
    }
        status_command i3status
}

# window rules, you can find the window class using xprop
for_window [class=".*"] border pixel 4
# WS 1 Browser
assign [class=Firefox|Transmission-gtk] $ws1
# WS 2 Dateien
assign [class=Pcmanfm|Thunar|File-roller] $ws2
# WS 3 Konsolen
assign [class=URxvt|kitty|Gnome-terminal] $ws3
# WS 4 Coding
assign [class=Geany|Evince|Gucharmap] $ws4
# WS 5 VMS
# WS 6 Office
assign [class=Soffice|libreoffice*] $ws6
# WS 7 E-Mail
# WS 8 Multimedia
assign [class=Audacity|Vlc|mpv|Ghb|Xfburn|Gimp*|Inkscape] $ws8
# WS 9 System
assign [class=Lxappearance|System-config-printer.py|Lxtask|GParted|Pavucontrol|Exo-helper*|Lxrandr|Arandr] $ws9

# Floating and Focus
for_window [class=Viewnior|feh|Audacious|File-roller|Lxappearance|Lxtask|Pavucontrol] floating enable
for_window [class=Gnome-terminal|kitty|URxvt|Firefox|Geany|Evince|Soffice|libreoffice*|mpv|Ghb|Xfburn|Gimp*|Inkscape|Vlc|Lxappearance|Audacity] focus
for_window [class=Xfburn|GParted|System-config-printer.py|Lxtask|Pavucontrol|Exo-helper*|Lxrandr|Arandr] focus

# colour of border, background, text, indicator, and child_border
client.focused              #bf616a #1E2127 #d8dee8 #bf616a #d8dee8
client.focused_inactive     #1E2127 #1E2127 #d8dee8 #1E2127 #1E2127
client.unfocused            #1E2127 #1E2127 #d8dee8 #1E2127 #1E2127
client.urgent               #1E2127 #1E2127 #d8dee8 #1E2127 #1E2127
client.placeholder          #1E2127 #1E2127 #d8dee8 #1E2127 #1E2127
client.background           #1E2127

#client.focused              #bf616a #2f343f #d8dee8 #bf616a #d8dee8
#client.focused_inactive     #2f343f #2f343f #d8dee8 #2f343f #2f343f
#client.unfocused            #2f343f #2f343f #d8dee8 #2f343f #2f343f
#client.urgent               #2f343f #2f343f #d8dee8 #2f343f #2f343f
#client.placeholder          #2f343f #2f343f #d8dee8 #2f343f #2f343f
#client.background           #2f343f
