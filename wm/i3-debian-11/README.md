
# Debian Bullseye 11 with i3 Window Manager

**NEW SOLO REPO (2022-07-24): https://gitlab.com/joebrandes/debian-i3-onedark.git**

New Tryouts in "Summer of code 2022" - change from WM Xmonad to i3 for *Quick Installs
and Use of a WM*.

Basisinstall Debian (with openSSH-Server): **76 MB RAM with 16 Tasks running!**

```
sudo apt install xorg xinit xsettingsd \
    i3 i3status i3lock-fancy \
    xterm rxvt kitty rofi suckless-tools dzen2 picom dunst \
    ranger highlight hsetroot scrot htop arandr nitrogen \
    pcmanfm thunar lxappearance mc vim curl wget gconf2 \
    pulseaudio alsa-utils xclip xsel feh viewnior yad xdotool
```

After Xorg and i3 stuff for Debian: **92 MB RAM with 22 Tasks running!**

Prepare XServer with `echo 'exec i3' > ~/.xsession` and try `startx`.

WM i3 running with minimal Configuration: **160 MB RAM with 33 Tasks!**

Link:
[More interesting Software stuff and complete usable i3 Enviroment](https://github.com/addy-dclxvi/i3-starterpack)
needs a bit more Fonts:

```
sudo apt install git fonts-noto fonts-mplus ...
```

With all that stuff we are coming to **210 MB RAM with 43 Tasks!**

## THEME: One Dark Pro

As a Theme Example - just happen to like it right now and
it seems i had a bit to much **Dracula Theme** for the last 2 to 3
years.

![One Dark Color Basic Palette](./pics/OneDarkPalette.png)

For a Look use
[Github Repo for One Gnome Terminal](https://github.com/denysdovhan/one-gnome-terminal/blob/master/COLORS)
or others: (here completed with some Color Adoptions)

```
One Dark                       One Light

Black:          #000000        Black:          #000000
Black (above)   #282C34        Black (above)   #282C34
Bright Black:   #5C6370        Bright Black:   #383A42
Red:            #E06C75        Red:            #E45649
Bright Red:     #E06C75        Bright Red:     #E45649
Green:          #98C379        Green:          #50A14F
Bright Green:   #98C379        Bright Green:   #50A14F
Yellow:         #D19A66        Yellow:         #986801
Bright Yellow:  #E5C07B        Bright Yellow:  #986801
Blue:           #61AFEF        Blue:           #4078F2
Light Blue:     #61AFEF        Light Blue:     #4078F2
Magenta:        #C678DD        Magenta:        #A626A4
Light Magenta:  #C678DD        Light Magenta:  #A626A4
Cyan:           #56B6C2        Cyan:           #0184BC
Light Cyan:     #56B6C2        Light Cyan:     #0184BC
White:          #ABB2BF        White:          #A0A1A7
Bright White:   #FFFFFF        Bright White:   #FFFFFF
Text:           #5C6370        Text:           #383A42
Bold Text:      #ABB2BF        Bold Text:      #A0A1A7
Selection:      #3A3F4B        Selection:      #3A3F4B
Cursor:         #5C6370        Cursor:         #383A42
Background:     #1E2127        Background:     #F9F9F9
```


## i3 - the classical Window Manager

and no - i am not using the *so called fancy i3-gaps* version ;-)

We need three config Folders for **i3**, **i3blocks** and **i3status**.
These are the core-components for i3. Later we will change the
status Bar using **polybar**.

So copy the Folders

``./userprofileData/.config/i3*`` to your own ``~/.config/i3*``

You may do these in the CLI or with Midnight Commander - feel free.

## GTK

Styling for GTK is provided by a Git Repo for One Dark with different
Theming from GTK to Cinnamon.

I provide the neccessary folder, but here come the Github Repo with the
up to date stuff:

[Github Repo AtomOneDarkTheme](https://github.com/UnnatShaneshwar/AtomOneDarkTheme)

So feel free to ``git clone https://github.com/UnnatShaneshwar/AtomOneDarkTheme `` to your ``~/.themes`` folder.

### Icons for One Dark GTK Theme

A nice *greenish* Icon Theme for OneDark:

So agein just ``git clone https://github.com/adhec/one-dark-icons.git`` to your ``~/.icons`` folder.

Changing of GTK Theme and Icons via Tool **lxappearence**.

### Background Pictures

I Provide Pictures in form of Folder ``./userprofileData/pictures``.

The setting is done via tool **nitrogen**.



## Gnome Terminal

Yeah i know: over 120 new packages and over 250 MB on the storage side of things.

But otherwise i have to make a lot of sacrifices using urxvt - and kitty is a bit
on the old side on Debian ...

So, let's do this: ``sudo apt install gnome-terminal``

**Styling with OneDark via Standard Colors possible and useful!**

Or user OneDark Theme for Gnome-Terminal - but gconf2 script needs *full* Gnome-Env.

[Get OneDark Theme for Gnome Terminal](https://github.com/denysdovhan/one-gnome-terminal)
(needs at least package **gconf2**)

If you trust Network-/Github-Script do:

``bash -c "$(curl -fsSL https://raw.githubusercontent.com/denysdovhan/gnome-terminal-one/master/one-dark.sh)"``
