# Debian Buster Installation (Server) with manual i3 and more
# as VBox on buero-2018
# -----------------------------------------------------------------------
# 2020-10-04 created my own Archive to start with on new i3-Installations
# i3-starterpack-joeb.tar.gz
# -----------------------------------------------------------------------
# Don't forget to install all necessary software:
# and please choose wisely - there is no must for all the stuff
# just some ideas...
# analyse the config files to make things work!
# -----------------------------------------------------------------------
# Linux Mint / Ubuntu
# -----------------------------------------------------------------------
sudo apt install i3 i3status i3lock i3lock-fancy \
    rxvt-unicode-256color rofi dmenu compton dunst \
    ranger highlight hsetroot scrot \
    thunar nemo nautilus lxappearance \
    pulseaudio alsa-utils xclip feh viewnior yad xdotool

# openSUSE 15.2
# -----------------------------------------------------------------------
sudo zypper install i3 i3status i3lock \
    rxvt-unicode rofi dmenu compton dunst \
    ranger highlight scrot \
    thunar nemo nautilus \
    pulseaudio alsa-utils xclip viewnior feh

# Problems opensuse 15.2
# ----------------------
# Paket 'i3lock-fancy' nicht gefunden.
# weglassen oder manuell...
# Paket 'rxvt-unicode-256color' nicht gefunden.
# nehme: rxvt-unicode
# Keine Anbieter von 'hsetroot' gefunden.
# nehme: feh


# for fonts see below: i like MesloLGS NF in .local/share/fonts/


# general remarks:
# -----------------------------------------------------------------------
# while experimenting with Window Manager: standard Terminal is rxvt
# e.g. https://medium.com/hacker-toolbelt/i3wm-on-debian-10-buster-c302420853b1
# which shows plane Debian Buster with i3 and xdm/lightdm, ...

# So new Configuration with: ~/.Xdefaults or ~/.Xresources

# e.g. or copy
# install PowerLine Font "MesloLGS NF" in .local/share/fonts/
# check with
# fc-list | grep -i MesloLGS
# ~/.Xdefaults
# URxvt*font: xft:MesloLGS NF:size=12


# gtk THEMES
# https://www.gnome-look.org/p/1308808/
# and ICONS
# https://www.pling.com/p/1309239/

