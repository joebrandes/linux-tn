# Rofi
Nice Menu - mostly per $super+D

Infos

*   [Github for Rofi](https://github.com/davatorium/rofi)
*   [Documentation for Rofi](https://github.com/davatorium/rofi/blob/next/doc/rofi.1.markdown)

Don't forget to implement Rofi in your i3 config:


    bindsym $super+d exec rofi -lines 15 -padding 18 -width 80 -location 0 -show drun -sidebar-mode -columns 3 -font 'MesloLGS NF 12' -show-icons -theme $HOME/.config/rofi/themes/solarized.rasi

Still trying to figure out about the config stuff...

