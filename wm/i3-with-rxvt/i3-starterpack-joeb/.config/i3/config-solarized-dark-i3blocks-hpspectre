# set modifier
set $super Mod4
set $alt Mod1

# set font
font pango: MesloLGS NF 12

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1: TRM  "
set $ws2 "2: WWW "
set $ws3 "3: FIL  "
set $ws4 "4: COD  "
set $ws5 "5: OFF  "
set $ws6 "6: FTP  "
set $ws7 "7: PDF  "
set $ws8 "8: GFX  "
set $ws9 "9: MUM  "
set $ws10 "10: VMS  "

# Use Mouse+$super to drag floating windows to their wanted position
floating_modifier $super

# mouse should not focus
focus_follows_mouse no

#autostart
exec --no-startup-id feh --randomize --bg-scale $HOME/Bilder/wallpaper/set03/*
#exec --no-startup-id hsetroot -center ~/Bild1.jpg
#exec --no-startup-id xsettingsd &
exec --no-startup-id compton -b

# start a terminal
bindsym $alt+Control+t exec urxvt
# bindsym $super+Return exec urxvt
# bindsym $super+Return exec i3-sensible-terminal

# start dmenu (a program launcher)
bindsym $super+Shift+d exec i3-dmenu-desktop --dmenu="dmenu -i -fn 'Noto Sans:size=12'"
bindsym $super+d exec rofi -lines 15 -padding 18 -width 80 -location 0 -show drun -sidebar-mode -columns 3 -font 'MesloLGS NF 12' -show-icons

# common apps keybinds
bindsym Print exec scrot 'cheese_%a-%d%b%y_%H.%M.png' -e 'viewnior ~/$f'
bindsym $super+l exec i3lock-fancy 
# bindsym $super+l exec i3lock -i ~/.wallpaper.png
bindsym $super+Shift+w exec firefox;workspace $ws2;focus
# bindsym $super+Shift+f exec thunar;workspace 3;focus
bindsym $super+e exec nemo;workspace $ws3;focus
# bindsym $super+Shift+g exec geany
bindsym $super+Shift+c exec codium;workspace $ws4;focus
bindsym $super+Shift+b exec Virtualbox;workspace $ws10;focus


#change volume - for standard volume control - without i3block blocklets
#bindsym XF86AudioRaiseVolume exec amixer -q set Master 5%+
#bindsym XF86AudioLowerVolume exec amixer -q set Master 5%-

# bindsym XF86AudioMute exec amixer set Master mute - for standard volume control - without i3block blocklets
#bindsym XF86AudioMute exec --no-startup-id amixer -q -D pulse sset Master toggle

#change volume with standard keys - without i3block blocklets
#bindsym $alt+Left exec amixer -q set Master 5%-
#bindsym $alt+Right exec amixer -q set Master 5%+
#bindsym $alt+Down exec --no-startup-id amixer -q -D pulse sset Master toggle

# i3blocks - change volume or toggle mute - i3blocks volume
#bindsym XF86AudioRaiseVolume exec amixer -q -D pulse sset Master 5%+ && pkill -RTMIN+10 i3blocks 
#bindsym XF86AudioLowerVolume exec amixer -q -D pulse sset Master 5%- && pkill -RTMIN+10 i3blocks
#bindsym XF86AudioMute exec amixer -q -D pulse sset Master toggle && pkill -RTMIN+10 i3blocks

#change volume with standard keys - i3blocks volume-pulseaudio
bindsym $alt+Left exec amixer -q -D pulse sset Master 5%- && pkill -RTMIN+1 i3blocks
bindsym $alt+Right exec amixer -q -D pulse sset Master 5%+ && pkill -RTMIN+1 i3blocks
bindsym $alt+Down exec amixer -q -D pulse sset Master toggle && pkill -RTMIN+1 i3blocks

# change volume or toggle mute - i3blocks volume-pulseaudio
bindsym XF86AudioRaiseVolume exec amixer -q -D pulse sset Master 5%+ && pkill -RTMIN+1 i3blocks 
bindsym XF86AudioLowerVolume exec amixer -q -D pulse sset Master 5%- && pkill -RTMIN+1 i3blocks
bindsym XF86AudioMute exec amixer -q -D pulse sset Master toggle && pkill -RTMIN+1 i3blocks



# music control
bindsym XF86AudioNext exec mpc next
bindsym XF86AudioPrev exec mpc prev
bindsym XF86AudioPlay exec mpc toggle
bindsym XF86AudioStop exec mpc stop

# kill focused window
bindsym $super+c kill
bindsym $alt+F4 kill

# change focus
bindsym $super+Left focus left
bindsym $super+Down focus down
bindsym $super+Up focus up
bindsym $super+Right focus right

# move focused window
bindsym $super+Shift+Left move left
bindsym $super+Shift+Down move down
bindsym $super+Shift+Up move up
bindsym $super+Shift+Right move right

# split in horizontal orientation
bindsym $super+h split h

# split in vertical orientation
bindsym $super+v split v

# split toggle
# bindsym $super+t split toggle

# enter fullscreen mode for the focused container
bindsym $super+f fullscreen toggle

# change container layout split
bindsym $super+s layout toggle split

# toggle tiling / floating
bindsym $super+space floating toggle

# change focus between tiling / floating windows
bindsym $super+Shift+space focus mode_toggle

# switch to workspace
bindsym $alt+Control+Right workspace next
bindsym $alt+Control+Left workspace prev
bindsym $super+1 workspace $ws1
bindsym $super+2 workspace $ws2
bindsym $super+3 workspace $ws3
bindsym $super+4 workspace $ws4
bindsym $super+5 workspace $ws5
bindsym $super+6 workspace $ws6
bindsym $super+7 workspace $ws7
bindsym $super+8 workspace $ws8
bindsym $super+9 workspace $ws9
bindsym $super+0 workspace $ws10

# move focused container to workspace
bindsym $super+Shift+1 move container to workspace $ws1
bindsym $super+Shift+2 move container to workspace $ws2
bindsym $super+Shift+3 move container to workspace $ws3
bindsym $super+Shift+4 move container to workspace $ws4
bindsym $super+Shift+5 move container to workspace $ws5
bindsym $super+Shift+6 move container to workspace $ws6
bindsym $super+Shift+7 move container to workspace $ws7
bindsym $super+Shift+8 move container to workspace $ws8
bindsym $super+Shift+9 move container to workspace $ws9
bindsym $super+Shift+0 move container to workspace $ws10

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $super+Shift+r restart

# exit i3
bindsym $super+q exec "i3-nagbar -t warning -m 'Wirklich beenden?' -b 'Ja' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        bindsym Left resize shrink width 5 px or 5 ppt
        bindsym Down resize grow height 5 px or 5 ppt
        bindsym Up resize shrink height 5 px or 5 ppt
        bindsym Right resize grow width 5 px or 5 ppt
        bindsym Return mode "default"
}
bindsym $super+r mode "resize"

# panel - some solarized dark stuff...
bar {
        position top
        colors {
        #background #2f343f
        #statusline #2f343f
        #separator #4b5262

        background #586e75
        statusline #fdf6e3
        separator #fdf6e3

        # colour of border, background, and text
        #focused_workspace       #2f343f #bf616a #d8dee8
        #active_workspace        #2f343f #2f343f #d8dee8
        #inactive_workspace      #2f343f #2f343f #d8dee8
        #urgent_workspacei       #2f343f #ebcb8b #2f343f
        #
        focused_workspace       #fdf6e3 #cb4616 #fdf6e3
        active_workspace        #fdf6e3 #cb4b16 #d8dee8
        inactive_workspace      #fdf6e3 #073642 #d8dee8
        urgent_workspacei       #fdf6e3 #ebcb8b #2f343f
    }
        # status_command i3status
        status_command i3blocks -c ~/.config/i3blocks/config-joeb
        # i3bar_command i3bar --transparency
        # tray_output none
}

# window rules, you can find the window class using xprop
for_window [class=".*"] border pixel 3

assign [class=URxvt|Xterm|Terminal] $ws1
assign [class=Firefox|Transmission-gtk] $ws2
assign [class=Nemo|Thunar|File-roller] $ws3
assign [class=Codium|Geany|Gucharmap] $ws4
assign [class=Evolution|Soffice|libreoffice*] $ws5
assign [class=Filezilla|Lxappearance|System-config-printer.py|Lxtask|GParted|Pavucontrol|Exo-helper*|Lxrandr|Arandr] $ws6
assign [class=Xreader|Evince] $ws7
assign [class=Gimp*|Inkscape] $ws8
assign [class=Audacity|vlc|mpv|Ghb|Xfburn] $ws9
assign [class=VirtualBox] $ws10


for_window [class=Viewnior|feh|yad|Audacious|File-roller|Lxappearance|Lxtask|Pavucontrol] floating enable
for_window [class=Xterm|Terminal] focus
for_window [class=VirtualBox|Codium|URxvt|Firefox|Filezilla|Geany|Evince|Soffice|libreoffice*|mpv|Ghb|Xfburn|Gimp*|Inkscape|vlc|Lxappearance|Audacity] focus
for_window [class=Xreader|Xfburn|GParted|System-config-printer.py|Lxtask|Pavucontrol|Exo-helper*|Lxrandr|Arandr] focus

# colour of border, background, text, indicator, and child_border
#client.focused              #bf616a #2f343f #d8dee8 #bf616a #d8dee8
#client.focused_inactive     #2f343f #2f343f #d8dee8 #2f343f #2f343f
#client.unfocused            #2f343f #2f343f #d8dee8 #2f343f #2f343f
#client.urgent               #2f343f #2f343f #d8dee8 #2f343f #2f343f
#client.placeholder          #2f343f #2f343f #d8dee8 #2f343f #2f343f
#client.background           #2f343f

client.focused              #cb4b16 #2f343f #d8dee8 #bf616a #cb4b16
#client.focused_inactive     #2f343f #2f343f #d8dee8 #2f343f #2f343f
client.unfocused            #586e75 #2f343f #d8dee8 #2f343f #586e75
#client.urgent               #2f343f #2f343f #d8dee8 #2f343f #2f343f
#client.placeholder          #2f343f #2f343f #d8dee8 #2f343f #2f343f
#client.background           #2f343f

# shutdown / restart / suspend...
# set $mode_system System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Ctrl+s) shutdown
set $mode_system System (l) lock, (e) logout, (r) reboot, (Ctrl+s) shutdown

mode "$mode_system" {
    bindsym l exec --no-startup-id i3lock-fancy, mode "default"
    bindsym e exec --no-startup-id i3-msg exit, mode "default"
    # bindsym s exec --no-startup-id i3lock-fancy && systemctl suspend, mode "default"
    # bindsym h exec --no-startup-id $i3lockwall && systemctl hibernate, mode "default"
    bindsym r exec --no-startup-id systemctl reboot, mode "default"
    bindsym Ctrl+s exec --no-startup-id systemctl poweroff -i, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

bindsym $super+BackSpace mode "$mode_system"
