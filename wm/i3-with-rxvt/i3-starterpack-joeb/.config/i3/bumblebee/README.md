# Bumblebee Status

*   [Github Bumblebee-Status](https://github.com/tobi-wan-kenobi/bumblebee-status)

## Installation via:

    # from PyPI (thanks @tony):
    # will install bumblebee-status into ~/.local/bin/bumblebee-status
    pip install --user bumblebee-status

Or (obviously) - depending on your OS Python Environment and Installations:

    pip3 install --user bumblebee-status

I like to copy the resulting Py-Scripts `bumblebee-status` and `bumblebee-ctl` in `.config/i3/bumblebee/..`
just to remind myself ;-)
c
