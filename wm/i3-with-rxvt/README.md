# i3 stuff

Window Manager always kind of interested me. But i never took *Deep Dives* in
them - so no efforts with them whatsoever.

In the last years i took the task on to build me an i3-Environment i can live on
on a daily basis.

![Bash and rxvt solarized dark styled](./screenshot-ex-2monitor-i3blocks-20201014.png)

Yes - i said it - i (first) choose **i3** instead of all the other brillant *best*
Window Manager other Nerds use.

I chose the WM-Flagshop and will not take on a discussion about the other WMs ;-)
No  - of course i would, because it is fun and in these days i live in **Xmonad**

As most WM i do my tests besides **Cinnamon** with **Linux Mint** or
fresh installs based on **Debian** or **Manjaro** or **ArcoLinux** with specific WM.

So this is obviously wide spread and i had many influences on **my road** to
**my i3**.

I organize this *private* i3-stuff in the sub-folder ``i3-starterpack-joeb`` which
has ideas like those in the *marcus-s* sub-folder.

I found *marcus-s* ideas thru i3 recherche on youtube and github:

*  https://github.com/marcusscomputer
*  https://www.youtube.com/c/marcusscomputer/videos

and started from there and left those infos in.

I implement my setting in ``i3-starterpack-joeb/.config`` and other sub-folder.

**IMPORTANT** - These are **no Copy & Paste solutions**

These are Code-Snippets to gradually implement in **your own Environment**! I'm not
even saying, that i'm using all the stuff.


## Links / Technologies


* [Joe Brandes](http://www.joe-brandes.de) - Web *Card* by Joe Brandes
* [Git](https://www.git-scm.com/) - Git Development

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
